# DEPENDENCIES #
### OpenCV  (> 2.4.x) ###
sudo apt-get install libopencv-dev 

### CGAL (>4.3) ###
download from https://gforge.inria.fr/frs/?group_id=52 the version 4.5, 

extract it into a folder, let's call it "FOLDER" 

open the file CGAL-4.5/include/CGAL/Triangulation_hierarchy_3.h

change line 654 from  CGAL_triangulation_precondition(!is_infinite(v)); to  CGAL_triangulation_precondition(!this->is_infinite(v));

open terminal and go into FOLDER/CGAL-4.5 (*cd FOLDER/CGAL-4.5*)

then on terminal:

*mkdir build

cd build

cmake ..

make

sudo make install*

then compile the lib and install

### Eigen3 (> 3.0.5) ###
sudo apt-get install libeigen3-dev

### lapack (> 3.3.x) ###
sudo apt-get install liblapacke-dev

### blas   (>1.2.x) ###
sudo apt-get install libblas-dev

### gmp ###
sudo apt-get install libgmp-dev

### OpenGL (> 3.x) ###
look in the web, it depends on your graphics card

### assimp (> 3.x) ###
sudo apt-get install libassimp-dev

### glfw ###
sudo apt-get install libglfw-dev

### GLUT ### 
sudo apt-get install freeglut3-dev

### Transformesh ###
svn checkout svn://scm.gforge.inria.fr/svnroot/mvviewer/

The password is anonsvn if requested

Then open terminal and go into FOLDER/mvviewer (*cd FOLDER/mvviewer*)

then on terminal:

*mkdir build

cd build

cmake ..

make*

Transformesh is compiled, now return into edgePointSpaceCarver project

and change into the CMakeLists.txt the line

link_directories(/home/andrea/workspaceC/mvviewer/build/src/TransforMesh)

to 

link_directories(FOLDER/mvviewer/build/src/TransforMesh)


# EXAMPLES #

In the example folder I put two image sequences from the KITTI dataset.