#version 420

in vec3 posC;
in vec4 shadowCoord;

layout (location=0) out float dist;

uniform sampler2DShadow shadowMap;
uniform vec3 cameraCenter;

void main(){

  float shadowCoeff = textureProj(shadowMap, shadowCoord);
  
  dist = shadowCoeff * length(cameraCenter-posC); 
 
}
