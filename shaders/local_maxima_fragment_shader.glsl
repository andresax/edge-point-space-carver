#version 420

in vec2 tex1Coord;   /*2D coordinate in camera 1*/
in vec4 shadowCoord1;

layout(location = 0) out float nonMaxima;

uniform float imW;
uniform float imH;
uniform int W;
uniform float th;
uniform sampler2D imageNCC;

void main(){
float w = float(W);

  float curRow, curCol, maxVal;
  vec4 res, resInit;
  
  resInit = texture(imageNCC, tex1Coord);
  maxVal = resInit.x;


  nonMaxima = 0.0;

  for(curRow = -w/imH; curRow < w/imH; curRow += 1.0/imH){
    for(curCol = -w/imW; curCol < w/imW; curCol += 1.0/imW){
      res = texture(imageNCC, tex1Coord + vec2(curCol, curRow));
      if(res.x >= maxVal){
        maxVal = res.x;
      }
    }
  }
  //if (resInit.x > th){
  if (maxVal == resInit.x && (resInit.x) > th){
    //nonMaxima = 1.0;
    nonMaxima = resInit.x;
  }
}
