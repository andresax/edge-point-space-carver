#version 420

in vec2 tex1Coord;   

layout(location = 0) out vec4 cumulativeSum;

uniform sampler2D imageCumulative;
uniform sampler2D imageCur;

void main(){

  vec4 resCur, cumSum;
  
  resCur = texture(imageCur, tex1Coord);
  cumSum = texture(imageCumulative, tex1Coord);

  if(resCur.x != -666666.000000f){
    cumulativeSum = cumSum + resCur;
  }else{
    cumulativeSum = cumSum + vec4(0.0f);
  }
}
