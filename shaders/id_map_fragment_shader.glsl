#version 420

in vec4 shadowCoord;         /*coordinate for shadow mapping (camera 2)*/
in float idV;
  
out float idOut;

uniform sampler2DShadow shadowMap;

void main(){
  
  float shadowCoeff = textureProj(shadowMap, shadowCoord);

  idOut = round(shadowCoeff * idV);
  
}