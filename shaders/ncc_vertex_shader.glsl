#version 420

in vec2 position;
in vec2 texcoord;

out vec4 projector1TexCoordV;   /*2D coordinate in camera 1*/
out vec2 tex2CoordV;   /*2D coordinate in camera 1*/
out vec2 tex1CoordV;   /*2D coordinate in camera 1*/

//uniform mat4 MVP1;

void main(){
 
  tex2CoordV=texcoord;
  //tex2Coord.y=-tex2Coord.y;//dino
  tex2CoordV.y=1.0-tex2CoordV.y;
  tex1CoordV=texcoord;
  gl_Position =  vec4(position,0.0,1.0);
}

