#version 420

layout (points) in;
layout (points, max_vertices=1) out;

in uint positionPointV[];       /*3D point position*/

out vec3 point;

uniform  float imW;
uniform  float imH;
uniform sampler2D image;
uniform sampler2D nccValue;
uniform sampler2D idImage;

void main(){

  float stepX = 1.0/imW;
  float stepY = 1.0/imH;

  float maxNcc = - 1000000.00f;

  float curX = 0.0;
  while(curX <= imW){
    float curY = 0.0;
    while(curY <= imH){
      float  ncc = texture(nccValue, vec2(curX, curY)).x;
      if(ncc > maxNcc){
        uint ids = texture(idImage, vec2(curX, curY)).x;
        if(ids == positionPointV[0]){
          point = texture(image, vec2(curX, curY));
        } 
      }
      curY = curY + stepY;
    }
    curX = curX + stepX;
  }

  EmitVertex();
}