#version 420
#define W_GAUS 7
#define M_PI 3.1415926535897932384626433832795

in vec2 tex1Coord;   /*2D coordinate in camera 1*/
in vec4 shadowCoord1;

layout(location = 0) out float filtered;

uniform float imW;
uniform float imH;
uniform float wGauss;
uniform float wMed;
uniform sampler2D imageNCC;

void main(){

  float curRow, curCol, sumWeight,sum1;
  float W = float(W_GAUS);

  float sigma = 1.0;
  sum1=0.0;
  sumWeight = 0.0;

  for(curRow = -W/imH; curRow < W/imH; curRow += 1.0/imH){
    for(curCol = -W/imW; curCol < W/imW; curCol += 1.0/imW){
        float gaussianWeight = (1/(sigma * 2*M_PI)) * exp (-(curCol*curCol + curRow * curRow)/(2*sigma*sigma));

        sum1 += gaussianWeight * texture(imageNCC, tex1Coord + vec2(curCol, curRow)).x;
        sumWeight += gaussianWeight;
      }
    }

  filtered = sum1/sumWeight;

}
