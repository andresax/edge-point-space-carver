myDataset_.nvm.cmvs #dataset with cmvs results
wall1_notSmooth_Added.ply # result before smoothing, with points downsampled from the fitted plane
wall1_notSmooth_notAdded.ply # result before smoothing, without points downsampled from the fitted plane
wall1_Smooth_Added.ply # result after smoothing, with points downsampled from the fitted plane
wall1_Smooth_notAdded.ply # result after smoothing, without points downsampled from the fitted plane
