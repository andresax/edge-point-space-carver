wall2_dense.nvm.cmvs #dataset with cmvs results
wall2_notSmooth_notAdded.ply # result before smoothing, without points downsampled from the fitted plane
wall2_Smooth_Added.ply # result after smoothing, with points downsampled from the fitted plane
wall2_Smooth_notAdded.ply # result after smoothing, without points downsampled from the fitted plane

