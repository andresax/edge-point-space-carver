/*
 * ManifoldReconstructionAndRefinement.cpp
 *
 *  Created on: 10/apr/2015
 *      Author: andrea
 */

#include "ManifoldReconstructionAndRefinement.h"

#include <glm.hpp>

#include "Matrix.h"
#include "utilities/conversionUtilities.hpp"
#include <Mesh.h>
#include "./utilities/Logger.h"

ManifoldReconstructionAndRefinement::ManifoldReconstructionAndRefinement(SweepConfiguration mySweepConf, ManifoldConfig myConf, Configuration conf) {

  conf_ = conf;
  myConf_ = myConf;
  mySweepConf_ = mySweepConf;
  manifolReconstructor_ = new ManifoldReconstructor(myConf_, conf_);
  //   meshSweeper_ = new MeshSweeperDense(mySweepConf_, false);
  meshSweeper_ = new MeshSweeper(mySweepConf_, false);

  idxCamGT_ = 0;
  withGT_ = false;
  loadGT_ = true;

  sigmaGT_ = 0.02;
  meshSweeper_->setSigmaGt(sigmaGT_);

  std::cout << "Sigma: " << sigmaGT_ << "m" << std::endl;



}

ManifoldReconstructionAndRefinement::~ManifoldReconstructionAndRefinement() {
}

void ManifoldReconstructionAndRefinement::run() {
  utilities::Logger logg;
  std::stringstream sout, fileout, s2out;
  sout << mySweepConf_.outputSweep.pathFirstMesh << ".off";
  fileout << mySweepConf_.outputSweep.pathMesh << "Log.txt";
  std::ofstream logFile(fileout.str());
  std::vector<msc::Matrix> p;
  std::vector<std::vector<int> > vV;

  logg.startEvent();
  manifolReconstructor_->setWeightsSpaceCarver(1.0, 0.150000, 0.0000);
  manifolReconstructor_->runFirstIteration();
  logg.endEventAndPrint("*****************first it: ", true);
  logFile << "*****************first it: " << logg.getLastDelta() << std::endl;
  //meshSweeper_->removeInvisible();
  manifolReconstructor_->computeMesh(false);
  manifolReconstructor_->getMesh().saveFormat(sout.str().c_str());
  s2out << mySweepConf_.outputSweep.pathFirstMesh << "Freespace.off";
  manifolReconstructor_->storeFreespaceMesh(s2out.str());

  if (withGT_) {
    if (loadGT_) {

      std::ifstream s;

      s.open(pathGT_.c_str(), std::ios::in);

      depthMapGT = cv::Mat(conf_.videoConfig.imageH, conf_.videoConfig.imageW, CV_32F);

      float mean = 0.0, tot = 0.0;
      for (int curR = 0; curR < conf_.videoConfig.imageH; ++curR) {

        std::string line;
        std::getline(s, line);
        std::istringstream iss(line);
        for (int curC = 0; curC < conf_.videoConfig.imageW; ++curC) {
          char c;
          iss >> depthMapGT.at<float>(curR, curC);
          iss >> c;
        }
      }
      /*cv::Mat temp, temp2;
       double max,min;
       cv::minMaxLoc(depthMapGT, &min, &max);
       temp2 = 255.0 * (depthMapGT-min)/(max-min);
       temp2.convertTo(temp,CV_8UC3);
       cv::imshow("depthMapGT",temp);
       cv::waitKey();*/
    } else {
      meshSweeper_->createDepthMapGT(pathGT_, idxCamGT_, depthMapGT);
    }
  }

  std::vector<msc::Matrix> temp;
  for (auto curPt : manifolReconstructor_->pointParser_->getPoints()) {
    temp.push_back(conversionUtilities::toLoviMatrix(curPt));
  }

  if (withGT_) {
    meshSweeper_->comparePoints(idxCamGT_, temp, depthMapGT, "INIT POINTS", logFile);
  }

  for (int curIt = 0; curIt < 1000; ++curIt) {
    std::cout << "***************************************************";
    std::cout << "ManifoldReconstructionAndRefinement iteration Num.: " << curIt << std::endl;

    logFile << "***************************************************";
    logFile << "ManifoldReconstructionAndRefinement iteration Num.: " << curIt << std::endl;

    std::stringstream sCurIt;
    sCurIt << curIt;

    /*mesh sweeper update with reconstruction infos*/
    meshSweeper_->setBoundingMaxVert(manifolReconstructor_->getBoundingMaxVert());
    meshSweeper_->setBoundingMinVert(manifolReconstructor_->getBoundingMinVert());
    meshSweeper_->setCurIter(curIt);
//    if (curIt % 200 == 190) {
//      meshSweeper_->setCleanMesh(true);
//    } else {
    meshSweeper_->setCleanMesh(false);
//    }

    meshSweeper_->restartWithNewMesh(manifolReconstructor_->getMesh());

    //meshSweeper_->smoothMesh();
    if (withGT_) {
      logg.startEvent();
      //meshSweeper_->removeInvisible();
      logg.endEventAndPrint("*****************removeInvisible: ", true);
      meshSweeper_->createDepthMap(idxCamGT_);
      meshSweeper_->compareDepthMap(depthMapGT, "MESH", logFile);
    }
//    std::stringstream s2;
//    s2 << mySweepConf_.outputSweep.pathMesh << "NotSmooth" << curIt << ".off";
//    meshSweeper_->saveCurMesh(s2.str().c_str());

    logg.startEvent();
    meshSweeper_->run(curIt);
    logg.endEventAndPrint("*****************meshSweeper_->run: ", true);

    logFile << "*****************meshSweeper_->run: " << logg.getLastDelta() << std::endl;

    if (withGT_) {
      meshSweeper_->comparePoints(idxCamGT_, meshSweeper_->getFilteredPoints(), depthMapGT, "POINTS", logFile);
    }
    /*Read points and visibility to perform batch manifold reconstruction*/
    for (auto v : meshSweeper_->getVis()) {
      vV.push_back(v);
    }
    for (auto curP : meshSweeper_->getFilteredPoints()) {
      p.push_back(conversionUtilities::toLoviMatrix(curP));
    }

    std::stringstream spt;

    spt << mySweepConf_.outputSweep.pathMesh << "_NewPoints" << curIt << "_" << ".obj";

    std::ofstream newpointsObj(spt.str());
    for (auto point : meshSweeper_->getFilteredPoints()) {
      newpointsObj << "v " << point[0] << " " << point[1] << " " << point[2] << std::endl;
    }
    newpointsObj.close();

    if (withGT_) {
      meshSweeper_->comparePoints(idxCamGT_, p, depthMapGT, "TOT POINTS without initial", logFile);
    }

    std::stringstream s;
    manifolReconstructor_->setWeightsSpaceCarver(1.0, 0.150, 0.0000);
    logg.startEvent();
    manifolReconstructor_->run(p, vV, false);
    logg.endEventAndPrint("*****************manifolReconstructor_->run: ", true);
    logFile << "*****************manifolReconstructor_->run: " << logg.getLastDelta() << std::endl;
//    manifolReconstructor_->run(p, vV, false);
    p.clear();
    vV.clear();
    manifolReconstructor_->computeMesh(false);
//exit(0);
  }
  logFile.close();

}
