/*
 * Logger.cpp
 *
 *  Created on: 10/apr/2015
 *      Author: andrea
 */

#include "Logger.h"
#include <iostream>
#include <iomanip>

namespace utilities {

Logger::Logger() {
  lastDelta_ = 0.0;
  enable_ = true;
}

Logger::~Logger() {
}

void Logger::startEvent() {
  struct timeval tmp;
  gettimeofday(&tmp, NULL);
  eventBeginnings.push_back(tmp);
}

void Logger::startEvent(std::string message, bool newParagraph) {
  if (enable_) {
    std::cout << message << " Started";
    if (newParagraph)
      std::cout << std::endl;
  }
  startEvent();
}

void Logger::endEvent() {
  struct timeval tmp, tmpEnd;
  gettimeofday(&tmpEnd, NULL);
  eventEnds.push_back(tmpEnd);
}

void Logger::endEvent(std::string message, bool newParagraph) {
  if (enable_) {
    std::cout << message << " ended ";
    if (newParagraph)
      std::cout << std::endl;
  }
  endEvent();
}

void Logger::printLastTime(bool newParagraph) {
  computeTime();
  if (enable_) {
    std::cout << "Done in " << lastDelta_ << "s ";
    if (newParagraph)
      std::cout << std::endl;
  }
}

void Logger::printLastTime(std::string message, bool newParagraph) {
  computeTime();
  if (enable_) {
    std::cout << message << " Done in " << lastDelta_ << "s ";
    if (newParagraph)
      std::cout << std::endl;
  }
}
void Logger::endEventAndPrint(bool newParagraph) {
  endEvent();
  printLastTime(newParagraph);
}

void Logger::endEventAndPrint(std::string message, bool newParagraph) {
  endEvent();
  printLastTime(message, newParagraph);
}

void Logger::enable() {
  enable_ = true;
}

void Logger::disable() {
  enable_ = false;
}

void Logger::computeTime() {
  struct timeval end, start;
  start = eventBeginnings.back();
  eventBeginnings.pop_back();
  end = eventEnds.back();
  eventEnds.pop_back();
  lastDelta_ = ((end.tv_sec - start.tv_sec) * 1000000u + end.tv_usec - start.tv_usec) / 1.e6;
}

void Logger::resetOutputPercision() {
  std::cout<<std::setprecision(5);
}
} /* namespace utilities */

