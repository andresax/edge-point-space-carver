/*
 * PathCreator.h
 *
 *  Created on: 22/set/2014
 *      Author: andrea
 */

#ifndef PATHCREATORSWEEP_H_
#define PATHCREATORSWEEP_H_



#include <string>
#include <iostream>
#include "../types.hpp"

class PathCreatorSweep {
  public:
  PathCreatorSweep(SpaceCarvingConfig &spaceCarvingConfig, SweepConfiguration &sweepConf, std::string prefix, std::string prefixFolder);
  PathCreatorSweep( SweepConfiguration &sweepConf, std::string prefix, std::string prefixFolder);
    virtual ~PathCreatorSweep();

    std::string getPathFirstMesh();

    std::string getPathMesh();

    void setPrefixAndSpaceC(const std::string& prefix,const std::string& prefixFolder, const SpaceCarvingConfig& spaceCarvingConfig,SweepConfiguration sweepConf) {
      prefix_Folder_ = prefixFolder;
      prefix_ = prefix;

      spaceCarvingConfig_ = spaceCarvingConfig;
      sweepConf_ = sweepConf;
      createDir();
    }


  private:
    SpaceCarvingConfig spaceCarvingConfig_;
    SweepConfiguration sweepConf_;
    std::string prefix_;
    std::string prefix_Folder_;
    std::string pathRootDir_;
    std::string pathRoot_;

    bool pathWithSpaceCarving_;



    void createDir();
};


#endif /* PathCreatorSweep_H_ */
