/*
 * ManifoldReconstructionAndRefinement.h
 *
 *  Created on: 10/apr/2015
 *      Author: andrea
 */

#ifndef MANIFOLDRECONSTRUCTIONANDREFINEMENT_H_
#define MANIFOLDRECONSTRUCTIONANDREFINEMENT_H_

#include "cam_parsers/CamParser.h"
#include <vector>
#include <string>
#include "ManifoldReconstructorEpflBundler.h"
#include "ManifoldReconstructor.h"
#include "types.hpp"
#include "SweepMesh/SweepConfigurator.h"
#include "SweepMesh/MeshSweeperDense.h"
#include "SweepMesh/MeshSweeper.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
class ManifoldReconstructionAndRefinement {
public:
  ManifoldReconstructionAndRefinement(SweepConfiguration mySweepConf, ManifoldConfig myConf, Configuration conf);
  virtual ~ManifoldReconstructionAndRefinement();

  void run();

  void setIdxCamGt(int idxCamGt) {
    idxCamGT_ = idxCamGt;
  }

  void setLoadGt(bool loadGt) {
    loadGT_ = loadGt;
  }

  void setPathGt(const std::string& pathGt) {
    pathGT_ = pathGt;
  }

  void setWithGt(bool withGt) {
    withGT_ = withGt;
  }

private:
  ManifoldReconstructor *manifolReconstructor_;
  Configuration conf_;
  ManifoldConfig myConf_;
  SweepConfiguration mySweepConf_;
//MeshSweeperDense *meshSweeper_;
  MeshSweeper *meshSweeper_;
  cv::Mat depthMapGT;
  float sigmaGT_;
  int idxCamGT_;
  bool loadGT_;
  bool withGT_;
  std::string pathGT_;

};

#endif /* MANIFOLDRECONSTRUCTIONANDREFINEMENT_H_ */
