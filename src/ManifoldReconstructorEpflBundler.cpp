/*
 * ManifoldReconstructorEpflBundler.cpp
 *
 *  Created on: 13/mar/2015
 *      Author: andrea
 */

#include "ManifoldReconstructorEpflBundler.h"
#include "utilities/conversionUtilities.hpp"
#include <sstream>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/eigen.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <Eigen/Core>

ManifoldReconstructorEpflBundler::ManifoldReconstructorEpflBundler(ManifoldConfig myConf, Configuration conf) {

  manifConf_ = myConf;
  conf_ = conf;
  spaceCarver_.setConfig(conf_);

  camParser_ = new CamParser(manifConf_.manifConfig.pathCamsPose);
  camParser_->parseFile();

  std::vector<std::string> pathImages = camParser_->getCamerasPaths();
  for (auto curPath : pathImages) {
    images_.push_back(cv::imread(curPath, 0));
  }

  pointParser_ = new PointsParserFromOut(manifConf_.manifConfig.pathInitPoints, myConf.videoConfig.imageW, myConf.videoConfig.imageH);
  pointParser_->parse();

}

ManifoldReconstructorEpflBundler::~ManifoldReconstructorEpflBundler() {
}

void storeManifoldMeshC(msc::FreespaceDelaunayManifold &spaceCarver, std::string nameFile) {

  std::cout << "storeManifoldMesh...";
  std::vector<msc::Matrix> m_arrModelPoints;
  std::vector<msc::Matrix> m_lstModelTris;
  spaceCarver.tetsToTrisSure(m_arrModelPoints, m_lstModelTris);
  spaceCarver.writeObj(nameFile.c_str(), m_arrModelPoints, m_lstModelTris);
  std::cout << "DONE." << std::endl;
}
void saveCurrentManifoldC(msc::FreespaceDelaunayManifold &spaceCarver, std::string suffix) {

  std::stringstream namefile;
  namefile << "ProvaHerzu_ManifinverseConicEnabled_" << suffix << ".obj";
  storeManifoldMeshC(spaceCarver, namefile.str());
}

void ManifoldReconstructorEpflBundler::runFirstIteration() {

  for (int curCamIdx = 0; curCamIdx < camParser_->getNumCameras(); ++curCamIdx) {
    CameraType curCam = camParser_->getCamerasList()[curCamIdx];
    spaceCarver_.addCamCenter(conversionUtilities::toLoviMatrix(curCam.center));
  }

  /*std::cout<<std::endl;*/
  /*
   for (auto curPt : pointParser_->getPoints()) {
   spaceCarver_.addPoint(conversionUtilities::toLoviMatrix(curPt));
   }*/
  std::ofstream fff("ciao.obj");
  for (int curP = 0; curP < pointParser_->getPoints().size(); ++curP) {
    if(pointParser_->getCamViewingPointN()[curP].size() >=2){
    std::vector<cv::Mat> temp;

    for (auto curC : pointParser_->getCamViewingPointN()[curP]) {

      temp.push_back(conversionUtilities::toOpenCVMatrix(camParser_->getCamerasList()[curC].cameraMatrix, false));
    }
    cv::Point3f tempP;
    estimate3dposition(temp, pointParser_->getPoint2DoncamViewingPoint()[curP],tempP);

    fff<<"v "<<tempP.x<<" "<<tempP<<" "<<tempP.y<<" "<<tempP<<std::endl;
    spaceCarver_.addPoint(conversionUtilities::toLoviMatrix(tempP));
    }
  }

  for (int curCamIdx = 0; curCamIdx < camParser_->getNumCameras(); ++curCamIdx) {

    std::cout << "spaceCarver_.addVisibilityPair cam:" << curCamIdx << std::endl;
    for (auto curPtIdx : pointParser_->getPointsVisibleFromCamN(curCamIdx)) {
      spaceCarver_.addVisibilityPair(curCamIdx, curPtIdx);
    }

    std::cout << "spaceCarver_.setShrinkEnabled(false) cam:" << curCamIdx << std::endl;
    spaceCarver_.setShrinkEnabled(false);

    std::cout << "spaceCarver_.IterateTetrahedronMethod cam:" << curCamIdx << std::endl;
    spaceCarver_.IterateTetrahedronMethod(curCamIdx);

  }

  std::cout << "spaceCarver_.regionGrowingBatch" << std::endl;
  spaceCarver_.regionGrowingBatch();

}

void ManifoldReconstructorEpflBundler::run(std::vector<msc::Matrix> point, std::vector<std::vector<int> > vis) {

  if (point.size() != vis.size()) {
    std::cerr << "point.size() != vis.size() not good" << std::endl;
    return;
  }

  for (int curPt = 0; curPt < point.size(); ++curPt) {
    int id = spaceCarver_.addPointWhere(point[curPt]);
    for (auto curVisPair : vis[curPt]) {
      spaceCarver_.addVisibilityPair(curVisPair, id);
    }
  }

  std::cout << "spaceCarver_.setShrinkEnabled(true)" << std::endl;
  spaceCarver_.setR(1000000.0f); //trick to shrink and recompute the whole manifold
  spaceCarver_.setShrinkEnabled(true);

  std::cout << "spaceCarver_.IterateTetrahedronMethod" << std::endl;
  spaceCarver_.IterateTetrahedronMethod(0);

  for (int curCamIdx = 1; curCamIdx < camParser_->getNumCameras(); ++curCamIdx) {
    spaceCarver_.setShrinkEnabled(false);
    std::cout << "spaceCarver_.IterateTetrahedronMethod cam:" << curCamIdx << std::endl;
    spaceCarver_.IterateTetrahedronMethod(curCamIdx);
  }

  std::cout << "spaceCarver_.regionGrowingBatch" << std::endl;
  spaceCarver_.regionGrowingIterative(0);
}

void ManifoldReconstructorEpflBundler::computeMesh() {

  std::string nameFile = "dump.off";
  std::cout << "storeManifoldMesh...";
//std::cout << "storeNonManifoldMesh...";
  std::vector<msc::Matrix> m_arrModelPoints;
  std::vector<msc::Matrix> m_lstModelTris;
  spaceCarver_.tetsToTrisSure(m_arrModelPoints, m_lstModelTris);
//spaceCarver_.tetsToTris(m_arrModelPoints, m_lstModelTris, 1);
  std::cout << "DONE." << std::endl;
  writeOFF(nameFile.c_str(), m_arrModelPoints, m_lstModelTris);

  mesh_.loadFormat(nameFile.c_str(), false);

}

void ManifoldReconstructorEpflBundler::writeOFF(const std::string filename, const std::vector<msc::Matrix> & points, const std::vector<msc::Matrix> & tris) {

  ofstream outfile;

// Open file
  outfile.open(filename.c_str());
  if (!outfile.is_open()) {
    cerr << "Unable to open file: " << filename << endl;
    return;
  }

  outfile << "OFF" << std::endl;
  outfile << points.size() << " " << tris.size() << " 0" << std::endl;

// Write out lines one by one.
  for (auto itPoints = points.begin(); itPoints != points.end(); itPoints++)
    outfile << static_cast<float>(itPoints->at(0)) << " " << static_cast<float>(itPoints->at(1)) << " " << static_cast<float>(itPoints->at(2)) << " " << endl;
  for (auto itTris = tris.begin(); itTris != tris.end(); itTris++)
    outfile << "3 " << static_cast<int>(round(itTris->at(0))) << " " << static_cast<int>(round(itTris->at(1))) << " " << static_cast<int>(round(itTris->at(2)))
        << std::endl;
// Close the file and return
  outfile.close();
}

void ManifoldReconstructorEpflBundler::setWeightsSpaceCarver(float w_1, float w_2, float w_3) {
  spaceCarver_.set1(w_1);
  spaceCarver_.set2(w_2);
  spaceCarver_.set3(w_3);
}

void ManifoldReconstructorEpflBundler::setThSpaceCarver(float th) {
  spaceCarver_.setNum(th);
}

void ManifoldReconstructorEpflBundler::estimate3dposition(std::vector<cv::Mat> &cameras, std::vector<cv::Point2f> &points2D, cv::Point3f &pt) {

  cv::Vec4f triangulated3DPointInitTemp;
  cv::Point3f triangulated3DPointInit, triangulated3DPoint;
  std::vector<cv::Mat> curCams;
  std::vector<cv::Point2f> curPoints;
  int lastCamIdx;
  cv::vector<cv::Point2f> firstPositionVec, lastPositionVec;
  cv::Point2f firstPosition, secondPosition;

  firstPositionVec.push_back(points2D[0]);

  lastCamIdx = cameras.size() - 1;
  lastPositionVec.push_back(points2D[lastCamIdx]);
  cv::triangulatePoints(cameras[0], cameras[lastCamIdx], firstPositionVec, lastPositionVec, triangulated3DPointInitTemp);
  triangulated3DPointInit.x = triangulated3DPointInitTemp[0] / triangulated3DPointInitTemp[3];
  triangulated3DPointInit.y = triangulated3DPointInitTemp[1] / triangulated3DPointInitTemp[3];
  triangulated3DPointInit.z = triangulated3DPointInitTemp[2] / triangulated3DPointInitTemp[3];

//***********************************************Gauss-Newton**********************************************//
  int resGN = GaussNewton(cameras, points2D, triangulated3DPointInit, triangulated3DPoint);

  if (resGN != -1) {
    pt.x = triangulated3DPoint.x;
    pt.y = triangulated3DPoint.y;
    pt.z = triangulated3DPoint.z;


  }else{

    pt.x = 0.0;
    pt.y = 0.0;
    pt.z = 0.0;
  }
}

int ManifoldReconstructorEpflBundler::GaussNewton(const std::vector<cv::Mat> &cameras, const std::vector<cv::Point2f> &points, cv::Point3d init3Dpoint,
    cv::Point3f &optimizedPoint) {
  int numMeasures = points.size();
  cv::Mat r = cv::Mat(numMeasures * 2, 1, CV_32F);

  cv::Mat curEstimate3DPoint = cv::Mat(3, 1, CV_32F);
  cv::Mat curEstimate3DPointH = cv::Mat(4, 1, CV_32F);
  curEstimate3DPoint.at<float>(0, 0) = init3Dpoint.x;
  curEstimate3DPoint.at<float>(1, 0) = init3Dpoint.y;
  curEstimate3DPoint.at<float>(2, 0) = init3Dpoint.z;

  cv::Mat J, H;
  double last_mse = 0;
  int i;
  for (i = 0; i < 50; i++) {

    double mse = 0;
    //compute residuals
    for (int curMeas = 0; curMeas < numMeasures; ++curMeas) {
      curEstimate3DPointH.at<float>(0, 0) = curEstimate3DPoint.at<float>(0, 0);
      curEstimate3DPointH.at<float>(1, 0) = curEstimate3DPoint.at<float>(1, 0);
      curEstimate3DPointH.at<float>(2, 0) = curEstimate3DPoint.at<float>(2, 0);
      curEstimate3DPointH.at<float>(3, 0) = 1.0;
      cv::Mat cur2DpositionH = cameras[curMeas] * curEstimate3DPointH;

      r.at<float>(2 * curMeas, 0) = ((points[curMeas].x - cur2DpositionH.at<float>(0, 0) / cur2DpositionH.at<float>(2, 0)));
      mse += r.at<float>(2 * curMeas, 0) * r.at<float>(2 * curMeas, 0);

      r.at<float>(2 * curMeas + 1, 0) = ((points[curMeas].y - cur2DpositionH.at<float>(1, 0) / cur2DpositionH.at<float>(2, 0)));
      mse += r.at<float>(2 * curMeas + 1, 0) * r.at<float>(2 * curMeas + 1, 0);

    }
    //mse = sqrt(mse)/(numMeasures*2);

    if (abs(mse / (numMeasures * 2) - last_mse) < 0.0000005) {
      break;
    }
    last_mse = mse / (numMeasures * 2);

    if (point2D3DJacobian(cameras, curEstimate3DPoint, J, H) == -1)
      return -1;

    curEstimate3DPoint += H.inv() * J.t() * r;

  }
  if (last_mse < 9/*3 pixels*/) {
    optimizedPoint.x = curEstimate3DPoint.at<float>(0, 0);
    optimizedPoint.y = curEstimate3DPoint.at<float>(1, 0);
    optimizedPoint.z = curEstimate3DPoint.at<float>(2, 0);
    return 1;
  } else {
    return -1;
  }
}

int ManifoldReconstructorEpflBundler::point2D3DJacobian(const std::vector<cv::Mat> &cameras, const cv::Mat &cur3Dpoint, cv::Mat &J, cv::Mat &hessian) {

  int numMeasures = cameras.size();
  cv::Mat cur3DPointHomog = cv::Mat(4, 1, CV_32F);
  ;
  cur3DPointHomog.at<float>(0, 0) = cur3Dpoint.at<float>(0, 0);
  cur3DPointHomog.at<float>(1, 0) = cur3Dpoint.at<float>(1, 0);
  cur3DPointHomog.at<float>(2, 0) = cur3Dpoint.at<float>(2, 0);
  cur3DPointHomog.at<float>(3, 0) = 1.0;

  J = cv::Mat(2 * numMeasures, 3, CV_32FC1);  //2 rows for each point: one for x, the other for y
  hessian = cv::Mat(3, 3, CV_32FC1);

  /*std::cout << "gdevre" <<std::endl;
   std::cout << cameras[0] <<std::endl;*/
  for (int curMeas = 0; curMeas < numMeasures; ++curMeas) {
    cv::Mat curReproj = cameras[curMeas] * cur3DPointHomog;
    float xH = curReproj.at<float>(0, 0);
    float yH = curReproj.at<float>(1, 0);
    float zH = curReproj.at<float>(2, 0);
    float p00 = cameras[curMeas].at<float>(0, 0);
    float p01 = cameras[curMeas].at<float>(0, 1);
    float p02 = cameras[curMeas].at<float>(0, 2);
    float p10 = cameras[curMeas].at<float>(1, 0);
    float p11 = cameras[curMeas].at<float>(1, 1);
    float p12 = cameras[curMeas].at<float>(1, 2);
    float p20 = cameras[curMeas].at<float>(2, 0);
    float p21 = cameras[curMeas].at<float>(2, 1);
    float p22 = cameras[curMeas].at<float>(2, 2);

    //d(P*X3D)/dX
    J.at<float>(2 * curMeas, 0) = (p00 * zH - p20 * xH) / (zH * zH);
    J.at<float>(2 * curMeas + 1, 0) = (p10 * zH - p20 * yH) / (zH * zH);

    //d(P*X3D)/dY
    J.at<float>(2 * curMeas, 1) = (p01 * zH - p21 * xH) / (zH * zH);
    J.at<float>(2 * curMeas + 1, 1) = (p11 * zH - p21 * yH) / (zH * zH);

    //d(P*X3D)/dZ
    J.at<float>(2 * curMeas, 2) = (p02 * zH - p22 * xH) / (zH * zH);
    J.at<float>(2 * curMeas + 1, 2) = (p12 * zH - p22 * yH) / (zH * zH);
  }

  hessian = J.t() * J;
  double d;
  d = cv::determinant(hessian);
  if (d < 0.00001) {
    //printf("doh");
    return -1;
  } else {
    return 1;
  }
}
/*
 //Mesh_Loader
 typedef Mesh_Loader<HalfedgeDS,Kernel> MeshLoader;
 void ManifoldReconstructorEpflBundler::computeMesh() {

 std::vector<msc::Matrix> m_arrModelPoints, m_lstModelTris;

 spaceCarver_.tetsToTrisSure(m_arrModelPoints, m_lstModelTris);


 ofstream outfile;




 // initialize mesh build structure and misc structures
 MeshLoader builder;
 std::vector<float>      newVertex;
 std::vector<int>      newVertexIdx;
 std::vector<std::string>  newVertexAttribTypes;
 int noVertexAttribs = MeshLoader::EVertexNoAttribs;
 int iIgnoreIdx=MeshLoader::EVertexIgnore;
 std::vector<int>      newTriangle;
 std::vector<std::string>  newFaceAttribTypes;
 int             iNoVertices;
 int             iNoTriangles;

 for(int i=0;i<noVertexAttribs;i++)
 {
 newVertex.push_back(0.0f);
 newVertexIdx.push_back(i);
 }
 for(int i=0;i<3;i++) newTriangle.push_back(-1);

 if(isOFF || isCOFF)
 {
 is >> iNoVertices; is >> iNoTriangles; is >> tmpValue;

 cout << "* Loading " << token << " mesh: " << filename << " (" << iNoTriangles << " facets, " << iNoVertices << " vertices)";
 if (isCOFF) cout << " (with colors).";
 cout << endl;

 float normFactor=1.0f;
 bool isNormFactorSet=false;

 // process vertices
 for ( int i=0; i < iNoVertices; i++ ) {
 for(int k=0;k<3;k++)
 is >> newVertex[k];
 if (isCOFF)
 {
 for(int k=3;k<6;k++)
 is >> newVertex[k];
 is >>tmpValue;

 if (!isNormFactorSet){
 if (tmpValue>1) normFactor=255.0f;
 isNormFactorSet = true;
 }
 for(int k=0;k<3;k++)
 newVertex[3 + k] /= normFactor;
 newVertex[MeshLoader::EVertexQuality] = (newVertex[3] + newVertex[4] + newVertex[5]) / 3;
 }
 builder.addVertex(newVertex);
 DEBUG_LOAD(
 cout << " - vertex " ;
 for(int i=0;i<6;i++)
 cout << newVertex[i] << " ";
 cout << endl;
 )

 }

 // process triangles
 for ( int i=0; i < iNoTriangles; i++ ) {
 int noVertsPerFacet;
 is >>noVertsPerFacet;

 if (noVertsPerFacet != 3)
 {
 cout << "\nWARNING: only triangular meshes are supported - ignoring facet";
 }

 for(int k=0;k<noVertsPerFacet;k++)
 {
 if (k<3)
 is >> newTriangle[k];
 else
 is >> tmpValue;

 }
 // skip any other text until the next line
 char buff[256];
 is.getline (buff,256);

 DEBUG_LOAD(
 cout << " - triangle " ;
 for(int i=0;i<3;i++)
 cout << newTriangle[i] << " ";
 cout << endl;
 )

 builder.addTriangle(newTriangle);

 }
 } // OFF



























 // Write out lines one by one.
 for (vector<Matrix>::const_iterator itPoints = points.begin(); itPoints != points.end(); itPoints++)
 outfile << "v " << static_cast<float>(itPoints->at(0)) << " " << static_cast<float>(itPoints->at(1)) << " " << static_cast<float>(itPoints->at(2))
 << endl;
 for (vector<Matrix>::const_iterator itTris = tris.begin(); itTris != tris.end(); itTris++)
 outfile << "f " << static_cast<int>(round(itTris->at(0)) + 1) << "//" << static_cast<int>(round(itTris->at(0)) + 1) << " "
 << static_cast<int>(round(itTris->at(1)) + 1) << "//" << static_cast<int>(round(itTris->at(1)) + 1) << " "
 << static_cast<int>(round(itTris->at(2)) + 1) << "//" << static_cast<int>(round(itTris->at(2)) + 1) << endl;
 // Close the file and return
 outfile.close();
 return 0;
 }

 void FreespaceDelaunayManifold::writeObj(ostream & outfile, const vector<Matrix> & points, const vector<Matrix> & tris) const {
 // Write out lines one by one.
 for (vector<Matrix>::const_iterator itPoints = points.begin(); itPoints != points.end(); itPoints++)
 outfile << "v " << itPoints->at(0) << " " << itPoints->at(1) << " " << itPoints->at(2) << endl;
 for (vector<Matrix>::const_iterator itTris = tris.begin(); itTris != tris.end(); itTris++)
 outfile << "f " << (round(itTris->at(0)) + 1) << " " << (round(itTris->at(1)) + 1) << " " << (round(itTris->at(2)) + 1) << endl;
 }*/
