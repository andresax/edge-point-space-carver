/*
 * CreateDepthMapFromMesh.h
 *
 *  Created on: 29/apr/2015
 *      Author: andrea
 */

#ifndef EPFLGTCOMPARE_CREATEDEPTHMAPFROMMESH_H_
#define EPFLGTCOMPARE_CREATEDEPTHMAPFROMMESH_H_

#include "Mesh.h"

#include <glm.hpp>
#include <opencv2/core/core.hpp>
#include <string>
#include <vector>
#include "../OpenGLWrapper/OpenGLProgram.h"
#include "../types.hpp"
#include "../OpenGLWrapper/TransformationController.h"
#include "../SweepMesh/DepthMapFromMeshShaderProgram.h"
#include "../SweepMesh/ShaderProgram.h"
#include "../SweepMesh/DepthShaderProgram.h"

class CreateDepthMapFromMesh : public OpenGLProgram{
public:
  CreateDepthMapFromMesh(std::string fileMesh, int imageWidth, int imageHeight, glm::mat4 &cameraM, glm::vec3 &cameraCenter);
  CreateDepthMapFromMesh(std::string fileMesh, int imageWidth, int imageHeight, CameraType cam, int curPart);
  virtual ~CreateDepthMapFromMesh();

  void run();
  void compute();
  void store(std::string fileName);
private:
  void initialize();
  void initShaders();
  void createVertexArrayBuffer();
  void createVertexArrayBufferMesh();
  glm::mat4 setCameraParamAndGetMvp(const CameraType &cam);
  void computeFromMesh();
  void dumpTextureOnCPU();
  void collectPointsFromPixels();
  void loadMesh(std::string path, int curPart);

  void computeFromVertices();
  void loadFromVertices(std::string &fileMesh);
  Mesh mesh_;
  glm::mat4 cameraM_;
  glm::vec3 cameraCenter_;
  CameraType cam_;
  std::vector<glm::vec3> vert_;
  std::vector<glm::vec3> fac_;

  TransformationController *cameraTransformationController_;
  bool isMesh_;
  std::vector<glm::vec3> vertices_;
  cv::Mat depthMap_;
  int numVert_;

  ShaderProgram *depthProgram_,*depthMapComp_;
  GLuint vertexBufferObj_,framebufferDepth_, depthTexture_, resultTex_;
  GLfloat *pixels_;

};

#endif /* EPFLGTCOMPARE_CREATEDEPTHMAPFROMMESH_H_ */
