/*
 * ManifoldReconstructor.cpp
 *
 *  Created on: 13/mar/2015
 *      Author: andrea
 */

#include "ManifoldReconstructor.h"
#include "utilities/conversionUtilities.hpp"
#include <sstream>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/eigen.hpp>
#include <Eigen/Core>

#include "./utilities/utilities.hpp"

ManifoldReconstructor::ManifoldReconstructor(ManifoldConfig myConf, Configuration conf) {

  manifConf_ = myConf;
  conf_ = conf;
  spaceCarver_ = new msc::FreespaceDelaunayManifold();
  spaceCarver_->setConfig(conf_);

  camParser_ = new CamParser(manifConf_.manifConfig.pathCamsPose);
  camParser_->parseFile();

  std::vector<std::string> pathImages = camParser_->getCamerasPaths();

  pointParser_ = new PointsParserFromOut(manifConf_.manifConfig.pathInitPoints, myConf.videoConfig.imageW, myConf.videoConfig.imageH);
  pointParser_->parse();

  stepImages = 1;
  offsetEnding = 272 - 100;
  offsetEnding = 68-60;
}

ManifoldReconstructor::~ManifoldReconstructor() {
}

void ManifoldReconstructor::storeManifoldMeshC(msc::FreespaceDelaunayManifold &spaceCarver, std::string nameFile) {

  std::cout << "storeManifoldMesh...";
  std::vector<msc::Matrix> m_arrModelPoints;
  std::vector<msc::Matrix> m_lstModelTris;
  spaceCarver.tetsToTrisSure(m_arrModelPoints, m_lstModelTris);
  spaceCarver.writeObj(nameFile.c_str(), m_arrModelPoints, m_lstModelTris);
  std::cout << "DONE." << std::endl;
}
void ManifoldReconstructor::saveCurrentManifoldC(msc::FreespaceDelaunayManifold &spaceCarver, std::string suffix) {

  std::stringstream namefile;
  namefile << "ProvaHerzu_ManifinverseConicEnabled_" << suffix << ".obj";
  storeManifoldMeshC(spaceCarver, namefile.str());
}
void ManifoldReconstructor::addSfMPointsAndCams(bool addPoints) {

  std::ofstream visFile("initVis_.ply");
  if (addPoints) {
//
    int numVisRays = 0;
    for (int curCamIdx = 0; curCamIdx < camParser_->getNumCameras(); ++curCamIdx) {
      numVisRays += pointParser_->getPointsVisibleFromCamN(curCamIdx).size();
    }

    visFile << "ply" << std::endl << "format ascii 1.0" << std::endl;
    visFile << "element vertex " << pointParser_->getPoints().size() + camParser_->getNumCameras() << std::endl;
    visFile << "property float x " << std::endl << "property float y " << std::endl << "property float z " << std::endl;
    visFile << "element edge " << numVisRays << std::endl;
    visFile << "property int vertex1 " << std::endl << "property int vertex2" << std::endl;
    visFile << "end_header" << std::endl;
  }

  for (int curCamIdx = 0; curCamIdx < camParser_->getNumCameras(); ++curCamIdx) {
    CameraType curCam = camParser_->getCamerasList()[curCamIdx];
    spaceCarver_->addCamCenter(conversionUtilities::toLoviMatrix(curCam.center));
    visFile << curCam.center.x << " " << curCam.center.y << " " << curCam.center.z << std::endl;
  }

  if (addPoints) {

    for (auto curPt : pointParser_->getPoints()) {
      if (curPt.x > boundingMaxVert.x)
        boundingMaxVert.x = 1.1 * curPt.x + 0.1;
      if (curPt.y > boundingMaxVert.y)
        boundingMaxVert.y = 1.1 * curPt.y + 0.1;
      if (curPt.z > boundingMaxVert.z)
        boundingMaxVert.z = 1.1 * curPt.z + 0.1;
      if (curPt.x < boundingMinVert.x)
        boundingMinVert.x = 1.1 * curPt.x - 0.1;
      if (curPt.y < boundingMinVert.y)
        boundingMinVert.y = 1.1 * curPt.y - 0.1;
      if (curPt.z < boundingMinVert.z)
        boundingMinVert.z = 1.1 * curPt.z - 0.1;
      visFile << curPt.x << " " << curPt.y << " " << curPt.z << std::endl;
      spaceCarver_->addPoint(conversionUtilities::toLoviMatrix(curPt));

    }

    for (int curCamIdx = 0; curCamIdx < camParser_->getNumCameras(); ++curCamIdx) {

      //std::cout << "spaceCarver_->addVisibilityPair cam:" << curCamIdx << " ";
      for (auto curPtIdx : pointParser_->getPointsVisibleFromCamN(curCamIdx)) {
        if (pointParser_->getCamViewingPointN(curPtIdx).size() > 1) {
          spaceCarver_->addVisibilityPair(curCamIdx, curPtIdx);
        }
//        msc::Matrix c = spaceCarver_->getCamCenter(curCamIdx);
//        msc::Matrix pt = spaceCarver_->getPoint(curPtIdx);
//        std::cout << "visPair: (cam (" << c[0] << ", " << c[1] << ", " << c[2] << "), "
//            <<" point(" << pt[0] << ", " << pt[1] << ", " <<pt[2] << "), ) " << std::endl;

        visFile << curCamIdx << " " << camParser_->getNumCameras() + curPtIdx << std::endl;
      }
//      std::cout << std::endl;
    }
    visFile.close();
  }

}

void ManifoldReconstructor::runFirstIteration() {
  boundingMaxVert = glm::vec3(-100000);
  boundingMinVert = glm::vec3(100000);
  addSfMPointsAndCams();

  //for (int curCamIdx = 200; curCamIdx <= 220; ++curCamIdx) {

//   for (int curCamIdx = 0; curCamIdx < camParser_->getNumCameras()-240; curCamIdx++) {
  for (int curCamIdx = 0; curCamIdx < camParser_->getNumCameras() - offsetEnding; curCamIdx += stepImages) {
    //std::cout << "spaceCarver_->setShrinkEnabled(false) cam:" << curCamIdx << std::endl;
    spaceCarver_->setShrinkEnabled(false);

    std::cout << "spaceCarver_->IterateTetrahedronMethod cam:" << curCamIdx << std::endl;
    if (conf_.spaceCarvingConfig.suboptimalMethod == 0) {
      spaceCarver_->IterateTetrahedronMethod(curCamIdx);
    } else {
      spaceCarver_->IterateTetrahedronMethod_suboptimal(curCamIdx);
    }

  }
  std::cout << "spaceCarver_->regionGrowingBatch" << std::endl;
  spaceCarver_->regionGrowingBatch();
  //exit(0);

}
void ManifoldReconstructor::storeFreespaceMesh(std::string nameFile) {

  std::cout << "Convert Tetrahedra to Triangles...";
  std::vector<msc::Matrix> m_arrModelPoints;
  std::vector<msc::Matrix> m_lstModelTris;
  spaceCarver_->tetsToTris(m_arrModelPoints, m_lstModelTris, (float) 1);
  writeOFF(nameFile.c_str(), m_arrModelPoints, m_lstModelTris);
  std::cout << "DONE." << std::endl;

}

void ManifoldReconstructor::run(std::vector<msc::Matrix> point, std::vector<std::vector<int> > vis, bool resetAll) {

  if (resetAll) {
    spaceCarver_->setShrinkEnabled(false);
    //std::cout << "resetAll triangulation" << std::endl;

    delete (spaceCarver_);
    spaceCarver_ = new msc::FreespaceDelaunayManifold();
    spaceCarver_->setConfig(conf_);
    boundingMaxVert = glm::vec3(-100000);
    boundingMinVert = glm::vec3(100000);

    addSfMPointsAndCams(true);

    /*spaceCarver_->resetReconstruction();*/

  }

  if (point.size() != vis.size()) {
    std::cerr << "point.size() != vis.size() not good" << std::endl;
    return;
  }

  for (int curPt = 0; curPt < point.size(); ++curPt) {
    if (point[curPt](0) > boundingMaxVert.x)
      boundingMaxVert.x = 1.1 * point[curPt](0) + 0.1;
    if (point[curPt](1) > boundingMaxVert.y)
      boundingMaxVert.y = 1.1 * point[curPt](1) + 0.1;
    if (point[curPt](2) > boundingMaxVert.z)
      boundingMaxVert.z = 1.1 * point[curPt](2) + 0.1;
    if (point[curPt](0) < boundingMinVert.x)
      boundingMinVert.x = 1.1 * point[curPt](0) - 0.1;
    if (point[curPt](1) < boundingMinVert.y)
      boundingMinVert.y = 1.1 * point[curPt](1) - 0.1;
    if (point[curPt](2) < boundingMinVert.z)
      boundingMinVert.z = 1.1 * point[curPt](2) - 0.1;
    int id = spaceCarver_->addPointWhere(point[curPt]);
    //std::cout << "Point" << point[curPt](0) << " " << point[curPt](1) << " " << point[curPt](2) << " ";
    for (auto curVisPair : vis[curPt]) {
      //std::cout << "visPair: (cam " << curVisPair << ", " << id << ") ";
      spaceCarver_->addVisibilityPair(curVisPair, id);
    }
    //std::cout << std::endl;
  }

  std::cout << "spaceCarver_->IterateTetrahedronMethod cam: 0" << std::flush;
//spaceCarver_->IterateTetrahedronMethod_suboptimal(0);
  //spaceCarver_->IterateTetrahedronMethod(0);

  //for (int curCamIdx = 200; curCamIdx <= 220; ++curCamIdx) {

  for (int curCamIdx = 0; curCamIdx < camParser_->getNumCameras() - offsetEnding; curCamIdx += stepImages) {
    if (!resetAll) {
      // std::cout << "spaceCarver_->setShrinkEnabled(true)" << std::endl;
      spaceCarver_->setShrinkEnabled(true);

    } else {
      spaceCarver_->setShrinkEnabled(false);
    }
    std::cout << " DONE, cam: " << curCamIdx << std::flush;
    if (conf_.spaceCarvingConfig.suboptimalMethod == 0) {
      spaceCarver_->IterateTetrahedronMethod(curCamIdx);
    } else {
      spaceCarver_->IterateTetrahedronMethod_suboptimal(curCamIdx);
    }
    //spaceCarver_->IterateTetrahedronMethod_suboptimal(curCamIdx);
  }
  std::cout << "DONE" << std::endl;
  if (!resetAll) {
    // std::cout << "spaceCarver_->regionGrowingBatch" << std::endl;
    spaceCarver_->regionGrowingIterative(0);
  } else {
    spaceCarver_->regionGrowingBatch();

  }
}

void ManifoldReconstructor::computeMesh(bool bounding) {

  computeMesh(-1, bounding);
}

void ManifoldReconstructor::computeMesh(int idx, bool bounding) {
  std::string nameFile;
  if (idx >= 0) {
    std::stringstream s;
    s << "dump" << idx << ".off";
    nameFile = s.str();
  } else {
    nameFile = "dump.off";
  }

  std::cout << "storeManifoldMesh...";
  std::vector<msc::Matrix> m_arrModelPoints;
  std::vector<msc::Matrix> m_lstModelTris;
  spaceCarver_->tetsToTrisSure(m_arrModelPoints, m_lstModelTris);
  std::cout << "DONE." << std::endl;

  if (bounding) {
    std::string nameFile2 = "dumpNot.off";
    writeOFF(nameFile2.c_str(), m_arrModelPoints, m_lstModelTris, true);
  }
  writeOFF(nameFile.c_str(), m_arrModelPoints, m_lstModelTris, false);

  mesh_.loadFormat(nameFile.c_str(), false);

}

void ManifoldReconstructor::writeOFF(const std::string filename, const std::vector<msc::Matrix> & points, const std::vector<msc::Matrix> & tris,
    bool bounding) {

  ofstream outfile;

// Open file
  outfile.open(filename.c_str());
  if (!outfile.is_open()) {
    cerr << "Unable to open file: " << filename << endl;
    return;
  }

  int numTr = tris.size();
  if (bounding) {
    numTr = 0;
    for (auto itTris = tris.begin(); itTris != tris.end(); itTris++) {
      int idxTr0 = static_cast<int>(round(itTris->at(0)));
      int idxTr1 = static_cast<int>(round(itTris->at(1)));
      int idxTr2 = static_cast<int>(round(itTris->at(2)));

      //      std::cout<< "-------------------------"<<std::endl;
      //      std::cout<< "idxTr0 "<<points[idxTr0](0)<<", "<<points[idxTr0](1)<<", "<<points[idxTr0](2)<<std::endl;
      //      std::cout<< "idxTr1 "<<points[idxTr1](0)<<", "<<points[idxTr1](1)<<", "<<points[idxTr1](2)<<std::endl;
      //      std::cout<< "idxTr2 "<<points[idxTr2](0)<<", "<<points[idxTr2](1)<<", "<<points[idxTr2](2)<<std::endl;
      //      std::cout<< "------------------------"<<std::endl;
      if (boundingMinVert.x <= points[idxTr0](0) && points[idxTr0](0) <= boundingMaxVert.x && boundingMinVert.x <= points[idxTr1](0)
          && points[idxTr1](0) <= boundingMaxVert.x && boundingMinVert.x <= points[idxTr2](0) && points[idxTr2](0) <= boundingMaxVert.x
          && boundingMinVert.y <= points[idxTr0](1) && points[idxTr0](1) <= boundingMaxVert.y && boundingMinVert.y <= points[idxTr1](1)
          && points[idxTr1](1) <= boundingMaxVert.y && boundingMinVert.y <= points[idxTr2](1) && points[idxTr2](1) <= boundingMaxVert.y
          && boundingMinVert.z <= points[idxTr0](2) && points[idxTr0](2) <= boundingMaxVert.z && boundingMinVert.z <= points[idxTr1](2)
          && points[idxTr1](2) <= boundingMaxVert.z && boundingMinVert.z <= points[idxTr2](2) && points[idxTr2](2) <= boundingMaxVert.z) {
        numTr++;
      }
    }
  }

  outfile << "OFF" << std::endl;
  outfile << points.size() << " " << numTr << " 0" << std::endl;

// Write out lines one by one.
  for (auto itPoints = points.begin(); itPoints != points.end(); itPoints++)
    outfile << static_cast<float>(itPoints->at(0)) << " " << static_cast<float>(itPoints->at(1)) << " " << static_cast<float>(itPoints->at(2)) << " " << endl;
  for (auto itTris = tris.begin(); itTris != tris.end(); itTris++) {
    int idxTr0 = static_cast<int>(round(itTris->at(0)));
    int idxTr1 = static_cast<int>(round(itTris->at(1)));
    int idxTr2 = static_cast<int>(round(itTris->at(2)));

    if (bounding) {
//      std::cout<< "-------------------------"<<std::endl;
//      std::cout<< "idxTr0 "<<points[idxTr0](0)<<", "<<points[idxTr0](1)<<", "<<points[idxTr0](2)<<std::endl;
//      std::cout<< "idxTr1 "<<points[idxTr1](0)<<", "<<points[idxTr1](1)<<", "<<points[idxTr1](2)<<std::endl;
//      std::cout<< "idxTr2 "<<points[idxTr2](0)<<", "<<points[idxTr2](1)<<", "<<points[idxTr2](2)<<std::endl;
//      std::cout<< "------------------------"<<std::endl;
      if (boundingMinVert.x <= points[idxTr0](0) && points[idxTr0](0) <= boundingMaxVert.x && boundingMinVert.x <= points[idxTr1](0)
          && points[idxTr1](0) <= boundingMaxVert.x && boundingMinVert.x <= points[idxTr2](0) && points[idxTr2](0) <= boundingMaxVert.x
          && boundingMinVert.y <= points[idxTr0](1) && points[idxTr0](1) <= boundingMaxVert.y && boundingMinVert.y <= points[idxTr1](1)
          && points[idxTr1](1) <= boundingMaxVert.y && boundingMinVert.y <= points[idxTr2](1) && points[idxTr2](1) <= boundingMaxVert.y
          && boundingMinVert.z <= points[idxTr0](2) && points[idxTr0](2) <= boundingMaxVert.z && boundingMinVert.z <= points[idxTr1](2)
          && points[idxTr1](2) <= boundingMaxVert.z && boundingMinVert.z <= points[idxTr2](2) && points[idxTr2](2) <= boundingMaxVert.z) {
        outfile << "3 " << idxTr0 << " " << idxTr1 << " " << idxTr2 << std::endl;
      }

    } else {
      outfile << "3 " << idxTr0 << " " << idxTr1 << " " << idxTr2 << std::endl;

    }
  }
// Close the file and return
  outfile.close();
}

void ManifoldReconstructor::setWeightsSpaceCarver(float w_1, float w_2, float w_3) {
  spaceCarver_->set1(w_1);
  spaceCarver_->set2(w_2);
  spaceCarver_->set3(w_3);
}

void ManifoldReconstructor::setThSpaceCarver(float th) {
  spaceCarver_->setNum(th);
}

void ManifoldReconstructor::setInfinityVal(float inf) {
  spaceCarver_->setInfiniteValue(inf);
}
