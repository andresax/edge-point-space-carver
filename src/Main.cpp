//  Copyright 2014 Andrea Romanoni
//
//  This file is part of edgePointSpaceCarver.
//
//  edgePointSpaceCarver is free software: you can redistribute it
//  and/or modify it under the terms of the GNU General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version see
//  <http://www.gnu.org/licenses/>..
//
//  edgePointSpaceCarver is distributed in the hope that it will be
//  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "types.hpp"
#include "EdgePointSpaceCarver.h"
#include "cam_parsers/KittiRectifiedCamParser.h"
#include "ManifoldReconstructorOld.h"
#include "utilities/PathCreator.h"
#include "utilities/utilities.hpp"
#include "utilities/PathCreatorSweep.h"
#include "SweepMesh/MeshSweeper.h"
#include "SweepMesh/MeshSweeperDense.h"
#include "SweepMesh/SweepConfigurator.h"
#include "ManifoldReconstructorConfigurator.h"
#include "ManifoldReconstructor.h"
#include "ManifoldReconstructionAndRefinement.h"
#include "epflGtCompare/CreateDepthMapFromMesh.h"

void readLineAndStore(std::ifstream &configFile, bool &value) {
  std::string line;
  int valueRead;
  std::getline(configFile, line);

  std::istringstream iss(line);
  iss >> valueRead;
  if (valueRead == 0) {
    value = false;
  } else {
    value = true;
  }
}

void readLineAndStore(std::ifstream &configFile, int &value) {
  std::string line;
  std::getline(configFile, line);

  std::istringstream iss(line);
  iss >> value;
}

void readLineAndStore(std::ifstream &configFile, double &value) {
  std::string line;
  std::getline(configFile, line);

  std::istringstream iss(line);
  iss >> value;
  std::cout << line << std::endl;
}

void readLineAndStore(std::ifstream &configFile, std::string &value) {
  std::string line;
  std::getline(configFile, line);

  std::istringstream iss(line);
  iss >> value;
  if (value.at(0) == '#') {
    value = std::string("");
  }
}

Configuration readConfig(std::string nameFile, std::string numKitti, int numCam) {

  Configuration myConf;
  std::string line;

  std::ifstream configFile(nameFile.c_str());

  std::getline(configFile, line);
  std::getline(configFile, line);

  readLineAndStore(configFile, myConf.videoConfig.baseNameImage);
  readLineAndStore(configFile, myConf.videoConfig.imageExtension);
  readLineAndStore(configFile, myConf.videoConfig.idxFirstFrame);
  readLineAndStore(configFile, myConf.videoConfig.digitIdxLength);
  readLineAndStore(configFile, myConf.videoConfig.idxLastFrame);
  if (myConf.videoConfig.idxLastFrame == 0) {
    myConf.videoConfig.idxLastFrame = numCam - 1;
  }
  readLineAndStore(configFile, myConf.videoConfig.imageH);
  readLineAndStore(configFile, myConf.videoConfig.imageW);

  std::getline(configFile, line);

  readLineAndStore(configFile, myConf.spaceCarvingConfig.keyFramePeriod);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.keyFramePeriodIteration);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.cannyHighThreshold);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.downsamplePeriod);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.maxGaussNewtonIteration);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.minDistBetweenTrackedPoints);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.maxEpipolarDist);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.minTrackLength);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.manifoldPeriod);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.inverseConicEnabled);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.probOrVoteThreshold);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.edgePointEnabled);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.firstGrowingFrame);
  readLineAndStore(configFile, myConf.outputSpaceCarving.enableSaveReconstr);
  readLineAndStore(configFile, myConf.outputSpaceCarving.enableSaveShrink);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.maxDistanceCamFeature);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.enableSuboptimalPolicy);
  readLineAndStore(configFile, myConf.spaceCarvingConfig.suboptimalMethod);

  return myConf;
}

int mainokk(int argc, char **argv) {

  std::ifstream camFile("/home/andrea/Scrivania/Datasets/EpflDataset/fountain_dense/CamerasP/0005.png.P");
  std::ifstream camFile2("/home/andrea/Scrivania/Datasets/EpflDataset/fountain_dense/CamerasKE/0005.png.camera");
//  std::ifstream camFile("/home/andrea/Scrivania/EpflDataset/herzjesu_dense/camerasP/0005.png.P");
//  std::ifstream camFile2("/home/andrea/Scrivania/EpflDataset/herzjesu_dense/CamerasKE/0005.png.camera");

  glm::vec3 center;
  glm::mat4 cam = glm::mat4(1.0);
  glm::mat3 camK = glm::mat3(1.0);
  glm::mat3 camR = glm::mat3(0.0);
  glm::vec3 camT = glm::vec3(0.0);
  std::cout << " CameraMatrix " << std::endl;
  for (int curR = 0; curR < 3; ++curR) {
    for (int curC = 0; curC < 4; ++curC) {
      camFile >> cam[curR][curC];
      std::cout << cam[curR][curC] << " ";
    }
    std::cout << std::endl;
  }

  std::string line;

  std::istringstream iss;
  std::getline(camFile2, line); //K row1
  iss.str(line);
  sscanf(iss.str().c_str(), "%f %f %f", &camK[0][0], &camK[0][1], &camK[0][2]);
  std::getline(camFile2, line); //K row2
  iss.str(line);
  sscanf(iss.str().c_str(), "%f %f %f", &camK[1][0], &camK[1][1], &camK[1][2]);
  std::getline(camFile2, line); //K row3
  iss.str(line);
  sscanf(iss.str().c_str(), "%f %f %f", &camK[2][0], &camK[2][1], &camK[2][2]);
  std::getline(camFile2, line); //K row4

  std::getline(camFile2, line); //R row1
  iss.str(line);
  sscanf(iss.str().c_str(), "%f %f %f", &camR[0][0], &camR[0][1], &camR[0][2]);
  std::getline(camFile2, line); //R row2
  iss.str(line);
  sscanf(iss.str().c_str(), "%f %f %f", &camR[1][0], &camR[1][1], &camR[1][2]);
  std::getline(camFile2, line); //R row3
  iss.str(line);
  sscanf(iss.str().c_str(), "%f %f %f", &camR[2][0], &camR[2][1], &camR[2][2]);

  std::getline(camFile2, line); //center

  iss.str(line);
  sscanf(iss.str().c_str(), "%f %f %f", &center[0], &center[1], &center[2]);

  //std::string pathIn("/home/andrea/Scrivania/EpflDataset/herzjesu.ply");
  //std::string pathIn("/home/andrea/workspaceC/edgePointSpaceCarver/prova.ply");

//  for (int part = 4; part < 5; ++part) {
  // int part = argv[1][0] - '0';

  std::string::size_type sz;   // alias of size_t

  int part = std::stoi(std::string(argv[1]), &sz);
  std::cout << "PArtNum:" << part << std::endl;

  CameraType camType;
  camType.center = center;
  camType.intrinsics = camK;
  camType.rotation = glm::transpose(camR);
  camType.translation = -center * glm::transpose(camR);

  utils::printMatrix("center ", center);
  utils::printMatrix("camR ", camR);
  utils::printMatrix("camK ", camK);
  utils::printMatrix("center ", center);
  utils::printMatrix("camType.rotation ", camType.rotation);
  utils::printMatrix("camType.translation ", camType.translation);
  utils::printMatrix("cam ", cam);

  CreateDepthMapFromMesh *c;

  std::stringstream s;
  s << "FountainMYLastOK" << part << ".txt";
  std::string pathOut(s.str().c_str());
//  std::string pathIn("/home/andrea/Scrivania/EpflDataset/herzjesu.ply");
  std::string pathIn("/home/andrea/workspaceC/fountainFinal.ply");


  c = new CreateDepthMapFromMesh(pathIn, 3072, 2048, camType, part);
  //c = new CreateDepthMapFromMesh(pathIn, 3072, 2048, cam, center);

  c->compute();
  c->store(pathOut);

}

//*************************************************************************************************/
//*************MESH REFINEMENT with MANIFOLD RECONSTRUCTION*************************************/
//*************************************************************************************************/
int main(int argc, char **argv) {

//    ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_herzuGT_ManifRec.config");
//   Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_herzu_ManifCarver.config", "", 8);
//   SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_herzuGT_sweep.config");
//
//   std::string pathGT("/home/andrea/workspaceC/edgePointSpaceCarver/depthGTHerzJesu.txt");
//   int idxCamDepth=5;
//   bool loadGT =true;
//  bool withGT = true;

//  ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_dino_ManifRec.config");
// Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_dino_ManifCarver.config", "", 8);
// SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_dino_sweep.config");
//
// std::string pathGT("");
// int idxCamDepth=5;
// bool loadGT =false;
//bool withGT = false;

//  ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_fountain_ManifRec.config");
//  Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_fountain_ManifCarver.config", "", 11);
//  SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_fountain_sweep.config");
//
//  std::string pathGT("/home/andrea/workspaceC/edgePointSpaceCarver/FountainDepthGT_2.txt");
//  int idxCamDepth = 3;
//  bool loadGT = true;
//  bool withGT = true;



  //ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_0095_ManifRec.config");
  //Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_0095_ManifCarver.config", "", 5);
  //SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_0095_sweep.config");
 //int idxCamDepth = 0;
 //std::string pathGT("");
 //bool loadGT = false;
 //bool withGT = false;


  ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_d_ManifRec.config");
  Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_d_ManifCarver.config", "", 5);
  SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_d_sweep.config");
 int idxCamDepth = 0;
 std::string pathGT("");
 bool loadGT = false;
 bool withGT = false;

/*
  ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_vienna2_ManifRec.config");
  Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_vienna2_ManifCarver.config", "", 5);
  SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_vienna2_sweep.config");
 int idxCamDepth = 0;
 std::string pathGT("");
 bool loadGT = false;
 bool withGT = false;
**/
//  ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_vienna3_ManifRec.config");
//  Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_vienna3_ManifCarver.config", "", 5);
//  SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_vienna3_sweep.config");
// int idxCamDepth = 0;
// std::string pathGT("");
// bool loadGT = false;
// bool withGT = false;




//   ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_simulatedPlane1_ManifRec.config");
//   Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_simulatedPlane1_ManifCarver.config", "", 2);
//   SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_simulatedPlane1_sweep.config");
//   int idxCamDepth = 0;
//   std::string pathGT("/home/andrea/workspaceC/simulatedDatasetCreator/plane1.off");
//   bool loadGT = false;
//   bool withGT = true;




   // ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_simulatedPlane_ManifRec.config");
   // Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_simulatedPlane_ManifCarver.config", "", 2);
   // SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_simulatedPlane_sweep.config");
   // int idxCamDepth = 0;
   // std::string pathGT("/home/andrea/workspaceC/simulatedDatasetCreator/piramid.off");
   // bool loadGT = false;
   // bool withGT = true;

  /* ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_indoor_wall1_ManifRec.config");
   Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_indoor_wall1_ManifCarver.config", "", 11);
   SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_indoor_wall1_sweep.config");
   int idxCamDepth = 0;
   std::string pathGT("");
   bool loadGT = false;
   bool withGT = false;*/
  /*
   ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_indoor_wall2_ManifRec.config");
   Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_indoor_wall2_ManifCarver.config", "", 5);
   SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_indoor_wall2_sweep.config");
   int idxCamDepth = 0;
   std::string pathGT("");
   bool loadGT = false;
   bool withGT = false;*/


//
// ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_Leuven_ManifRec.config");
// Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_Leuven_ManifCarver.config", "", 5);
// SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_Leuven_sweep.config");
//int idxCamDepth = 0;
//std::string pathGT("");
//bool loadGT = false;
//bool withGT = false;


//    ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_0095Stereo_ManifRec.config");
//    Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_0095Stereo_ManifCarver.config", "", 5);
//    SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_0095Stereo_sweep.config");
//   int idxCamDepth = 0;
//   std::string pathGT("");
//   bool loadGT = false;
//   bool withGT = false;
  //  ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_castle_entry_ManifRec.config");
  //  Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_castle_entry_ManifCarver.config", "", 11);
  //  SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_castle_entry_sweep.config");
  //   int idxCamDepth = 0;
  //   std::string pathGT("");
  //  bool loadGT =false;
  // bool withGT = false;
  //  ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_notredame_ManifRec.config");
  //  Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_notredame_ManifCarver.config", "", 11);
  //  SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_notredame_sweep.config");
  //   int idxCamDepth = 0;
  //   std::string pathGT("");
  //  bool loadGT =false;
  // bool withGT = false;

  ManifoldConfig myConf2 = mrc.parseConfigFile();
  SweepConfiguration sweepC = c.parseConfigFile();

  PathCreatorSweep pcs(sweepC, std::string("SweepGT_"), std::string("ResultsGTSweep"));

  sweepC.outputSweep.pathFirstMesh = pcs.getPathFirstMesh();
  sweepC.outputSweep.pathMesh = pcs.getPathMesh();

  ManifoldReconstructionAndRefinement mrar(sweepC, myConf2, myConf);

  mrar.setIdxCamGt(idxCamDepth);
  mrar.setPathGt(pathGT);
  mrar.setLoadGt(loadGT);
  mrar.setWithGt(withGT);

  mrar.run();
  return 0;
}

//*************************************************************************************************/
//*************MESH REFINEMENT without MANIFOLD RECONSTRUCTION*************************************/
//*************************************************************************************************/
int main555(int argc, char **argv) {
  //SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/ConfigFile_dino_sweep_without.config");
  SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/ConfigFile_herzu_sweepGT_without.config");
  SweepConfiguration myConf = c.parseConfigFile();

  PathCreatorSweep pcs(myConf, std::string("SweepGT_"), std::string("ResultsGTSweep"));

  myConf.outputSweep.pathFirstMesh = pcs.getPathFirstMesh();
  myConf.outputSweep.pathMesh = pcs.getPathMesh();

  MeshSweeper mr(myConf);
  //mr.smoothMesh();
  for (int curIt = 0; curIt < 500; ++curIt) {
    std::stringstream s1, s2, s3;
    s1 << myConf.outputSweep.pathMesh << "Before" << curIt << ".off";
    s2 << myConf.outputSweep.pathMesh << "After" << curIt << ".off";
    s3 << myConf.outputSweep.pathMesh << "Points" << curIt << ".obj";

    if (curIt % 2 == 0) {
      mr.setCleanMesh(true);
    } else {
      mr.setCleanMesh(false);
    }

    mr.run();
    mr.saveCurPoints(s3.str());

    mr.saveCurMesh(s1.str());
    mr.updateWithCurPoints();
    mr.saveCurMesh(s2.str());
  }
  return 0;
}

//*************************************************************************************************/
//***************************************MESH REFINEMENT DENSE*************************************/
//*************************************************************************************************/
int main5(int argc, char **argv) {
  SweepConfigurator c("/home/andrea/workspaceC/edgePointSpaceCarver/config/configMVS/Conf_fountain_sweep.config");
  SweepConfiguration myConf = c.parseConfigFile();

  MeshSweeperDense mr(myConf);

  mr.run();
  return 0;
}

//*************************************************************************************************/
//************************************MANIFOLD RECONSTRUCTION**************************************/
//*************************************************************************************************/
int main4(int argc, char **argv) {
  ManifoldReconstructorConfigurator mrc("/home/andrea/workspaceC/edgePointSpaceCarver/config/ConfigFile_herzu_manifRec.config");
  Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/herzu.config", "", 8);
  ManifoldConfig myConf2 = mrc.parseConfigFile();
  ManifoldReconstructor mr(myConf2, myConf);

  mr.runFirstIteration();
  mr.computeMesh();
  return 0;
}

//*************************************************************************************************/
//*************MANIFOLD RECONSTRUCTION OLD WITHOUT REAL CONFIG*************************************/
//*************************************************************************************************/
int main2(int argc, char **argv) {

  Configuration myConf = readConfig("/home/andrea/workspaceC/edgePointSpaceCarver/config/herzu.config", "", 8);
  ManifoldReconstructorOld mr(myConf);

  mr.run();
  return 0;
}

//*************************************************************************************************/
//************************************EDGE-POINT RECONSTRUCTION************************************/
//*************************************************************************************************/

int main3(int argc, char** argv) {
  std::string numKitti;
  std::string arg1, arg2;
  if (argc > 2) {
    arg1 = argv[1];
    arg2 = argv[2];
    std::cout << "Two arguments are needed: the first is the path of the configuration file, "
        << "the second one is the number of the KITTI sequence, do not omit the '0' (i.e., a right argument is 0095, not 95)" << std::endl;
  }

  PathCreator pathcreator(arg2);

  KittiRectifiedCamParser *kittiParser = new KittiRectifiedCamParser(pathcreator.getPathKittiOut());
  kittiParser->parseFile();
  std::vector<CameraRect> cameras = kittiParser->getCamerasList();

  Configuration myConf = readConfig(arg1, numKitti, cameras.size());

  pathcreator.setPrefixAndSpaceC(std::string("ReconstKitti_"), std::string("Results"), myConf.spaceCarvingConfig);
  myConf.videoConfig.folderImage = pathcreator.getPathFolderImage();

  myConf.outputSpaceCarving.pathToSave = pathcreator.getPathToSave();
  myConf.outputSpaceCarving.pathLog = pathcreator.getPathLog();
  myConf.outputSpaceCarving.pathLogPoints = pathcreator.getPathLogPoints();
  myConf.outputSpaceCarving.pathStats = pathcreator.getPathStats();
  myConf.outputSpaceCarving.pathStatsManifold = pathcreator.getPathStatsManifold();

  std::stringstream filepath;
  filepath << myConf.outputSpaceCarving.pathToSave << "configurationUsed.txt";
  std::ofstream fileConf(filepath.str().c_str());
  fileConf << myConf.toString();
  std::cout << myConf.toString() << std::endl;
  fileConf.close();

  EdgePointSpaceCarver spaceCarver(myConf, cameras);
  spaceCarver.run();

  return 0;
}

