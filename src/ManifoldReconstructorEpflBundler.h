/*
 * ManifoldReconstructor.h
 *
 *  Created on: 13/mar/2015
 *      Author: andrea
 */

#ifndef MANIFOLDRECONSTRUCTOREPFL_H_
#define MANIFOLDRECONSTRUCTOREPFL_H_
#include "cam_parsers/CamParser.h"
#include "cam_parsers/PointsParserFromOut.h"
#include <vector>
#include "FreespaceDelaunayManifold.h"
#include "types.hpp"
#include <Mesh.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

class ManifoldReconstructorEpflBundler {
public:
  ManifoldReconstructorEpflBundler(ManifoldConfig myConf, Configuration conf);
  virtual ~ManifoldReconstructorEpflBundler();

  void runFirstIteration();
  void run(std::vector<msc::Matrix> points, std::vector<std::vector<int> > vis);
  void computeMesh();

  void setWeightsSpaceCarver(float w_1, float w_2, float w_3);
  void setThSpaceCarver(float th);

  Mesh& getMesh() {
    return mesh_;
  }

  // void run();
private:
  void writeOFF(const std::string filename, const std::vector<msc::Matrix> & points, const std::vector<msc::Matrix> & tris);

  void estimate3dposition(std::vector<cv::Mat> &cameras, std::vector<cv::Point2f> &points2D, cv::Point3f &pt);
  int GaussNewton(const std::vector<cv::Mat> &cameras, const std::vector<cv::Point2f> &points, cv::Point3d init3Dpoint,
      cv::Point3f &optimizedPoint);

  int point2D3DJacobian(const std::vector<cv::Mat> &cameras, const cv::Mat &cur3Dpoint, cv::Mat &J, cv::Mat &hessian) ;

  Mesh mesh_;
  CamParser *camParser_;
  PointsParserFromOut *pointParser_;
  std::vector<cv::Mat> images_;
  msc::FreespaceDelaunayManifold spaceCarver_;
  ManifoldConfig manifConf_;
  Configuration conf_;

};

#endif /* MANIFOLDRECONSTRUCTOR_H_ */
