/*
 * GridPoints.h
 *
 *  Created on: 28/mag/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_GRIDPOINTS_H_
#define SWEEPMESH_GRIDPOINTS_H_

#include <vector>
#include <glm.hpp>
#include "../types.hpp"


class GridPoints {
public:
  GridPoints();
  virtual ~GridPoints();

  void setMaxPoints(int maxPoints) {
    maxPoints_ = maxPoints;
  }

  void setNumTilesCol(int numTilesCol) {
    numTilesCol_ = numTilesCol;
  }

  void setNumTilesRow(int numTilesRow) {
    numTilesRow_ = numTilesRow;
  }

  void addPoint(glm::mat4 camMatrix, PointNccSweep tmpPoint);

  void reset();

  void  getBestPoints(std::vector<PointNccSweep> &points);

private:
  static bool comparePoints(PointNccSweep p1, PointNccSweep p2);

  std::vector<std::vector<std::vector<PointNccSweep> > > grid_;

  int numTilesRow_;
  int numTilesCol_;

  int maxPoints_;
};

#endif /* SWEEPMESH_GRIDPOINTS_H_ */
