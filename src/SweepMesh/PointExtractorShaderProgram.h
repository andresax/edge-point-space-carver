/*
 * PointExtractorShaderProgram.h
 *
 *  Created on: 08/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_POINTEXTRACTORSHADERPROGRAM_H_
#define SWEEPMESH_POINTEXTRACTORSHADERPROGRAM_H_

#include "ShaderProgram.h"
#include "../utilities/Logger.h"
#include <glm.hpp>
#include <vector>

class PointExtractorShaderProgram : public ShaderProgram {
public:
  PointExtractorShaderProgram(int imageWidth, int imageHeight);
  PointExtractorShaderProgram(int imageWidth, int imageHeight, std::string vert_sh, std::string frag_sh);
  virtual ~PointExtractorShaderProgram();
  void initializeFramebufAndTex(GLuint &texId);

  void compute(bool renderFrameBuf = false);
  void resetTex();

  //int collectNewPoints(std::vector<glm::vec3> &points);
  int collectNewPoints(std::vector<glm::vec4> &points);
  void collectNewPoints();

  void filterPoints();

  void setImagePointsKept(GLuint imagePointsKept) {
    imagePointsKept_ = imagePointsKept;
  }

  void setDepthTexture(GLuint depthTexture) {
    depthTexture_ = depthTexture;
  }

  void setMvp(const glm::mat4& mvp) {
    mvp_ = mvp;
  }

  void setNormalBuffer(GLuint normalBuffer) {
    normalBuffer_ = normalBuffer;
  }

  void setAlpha(float alpha) {
    alpha_ = alpha;
  }

  void setIdBuffer(GLuint idBuffer) {
    idBuffer_ = idBuffer;
  }

  const std::vector<glm::vec3>& getPoints() const {
    return points_;
  }

  void setCurIds(const std::vector<GLuint>& curIds) {
    curIds_ = curIds;
  }

  void setCurNcc(const std::vector<GLfloat>& curNcc) {
    curNcc_ = curNcc;
  }

  void setCamCenter(const glm::vec3& camCenter) {
    camCenter_ = camCenter;
  }

  void setTh(GLfloat th) {
    th_ = th;
  }

private:
  void init();

  void createAttributes();
  void createUniforms();

  void dumpTextureOnCPU();
  void collectPointsFromPixels();
  void collectPointsNccIdFromPixels();

  GLuint framebuffer_;
  /*uniforms id*/
  GLuint imageTexId_, mvpId_, shadowMapId_;
  /*tex id*/
  GLuint pointsExtractedTexId_, imagePointsKept_;
  /*attributes id*/
  GLuint posId_;
  std::vector<glm::vec4> pointsAndIdNcc_;
  std::vector<glm::vec3> points_;
  std::vector<GLfloat> curNcc_;
  std::vector<GLuint> curIds_;

  GLuint normAttrib_, idAttrib_;
  GLuint normalBuffer_;
  GLuint idBuffer_;
  GLuint alphaId_, nccTex_, camCenterID_;
  glm::vec3 camCenter_;
  float alpha_;

  GLuint depthTexture_;
  glm::mat4 mvp_;
  GLfloat *pixels_;
  int ch_;
  GLuint  thId_, imHId_, imWId_;
  GLfloat th_, imH_, imW_;

  utilities::Logger logger;

  std::string vert_sh_, frag_sh_;

  GLuint tmpdeptht;
};

#endif /* SWEEPMESH_POINTEXTRACTORSHADERPROGRAM_H_ */
