/*
 * CollectLocalMaximaShaderProgram.cpp
 *
 *  Created on: 15/apr/2015
 *      Author: andrea
 */

#include "CollectLocalMaximaShaderProgram.h"
#include "../utilities/Logger.h"

CollectLocalMaximaShaderProgram::CollectLocalMaximaShaderProgram(int imageWidth, int imageHeight) :
    ShaderProgram(imageWidth, imageHeight) {
  feedbackBuffer_ = feedbackLength_ = -1;

  imWid_ = imHid_ = imageMaximaTexId_ = -1;
  imageMaximaTex_ = -1;

  pointsId_ = -1;
  query_ = -1;

}

CollectLocalMaximaShaderProgram::~CollectLocalMaximaShaderProgram() {
}

void CollectLocalMaximaShaderProgram::createTransformFeedback(int length) {
  glGenBuffers(1, &feedbackBuffer_);
  glBindBuffer(GL_ARRAY_BUFFER, feedbackBuffer_);
  glBufferData(GL_ARRAY_BUFFER, length * sizeof(glm::vec4), nullptr, GL_DYNAMIC_READ);

  glGenQueries(1, &query_);
}

void CollectLocalMaximaShaderProgram::compute(bool renderFrameBuf) {
  utilities::Logger log;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  shaderManager_.enable();

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, imageMaximaTex_);
  glUniform1i(imageMaximaTexId_, 0);

  glEnableVertexAttribArray(pointsId_);
  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glVertexAttribPointer(pointsId_, 2, GL_FLOAT, GL_FALSE, 0, 0);

  glEnable(GL_RASTERIZER_DISCARD);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, arrayBufferObj_);

  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, feedbackBuffer_);/*addd*/
  glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query_);
  glBeginTransformFeedback(GL_POINTS);
  log.startEvent("CollectLocalMaximaShaderProgram::compute::glDrawArrays");
  if (useElements_Indices) {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsBufferObj_);
    glDrawElements(GL_TRIANGLES, numElements_, GL_UNSIGNED_INT, 0);
  } else {
    glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
    glDrawArrays(GL_TRIANGLES, 0, sizeArray_);
  }
  glEndTransformFeedback();
  glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

  glDisableVertexAttribArray(pointsId_);

  glFlush();
  glDisable(GL_RASTERIZER_DISCARD);

  log.endEventAndPrint();
  GLuint primitives;
  glGetQueryObjectuiv(query_, GL_QUERY_RESULT, &primitives);

  feedbackTr.resize(primitives, glm::vec4(0.0));
  feedbackLength_ = primitives;

  log.startEvent("CollectLocalMaximaShaderProgram::compute::glGetBufferSubData");
  glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, feedbackLength_ * sizeof(glm::vec4), &feedbackTr[0]);
  log.endEventAndPrint();
}

void CollectLocalMaximaShaderProgram::init() {
  shaderManager_.init();
  shaderManager_.addShader(GL_VERTEX_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/collect_local_maxima_vertex_shader.glsl");
  shaderManager_.addShader(GL_GEOMETRY_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/collect_local_maxima_geometry_shader.glsl");
  shaderManager_.addFeedbackTransform("pointOK");
  shaderManager_.finalize();
}

void CollectLocalMaximaShaderProgram::createAttributes() {
  pointsId_ = shaderManager_.getAttribLocation("point");

}

void CollectLocalMaximaShaderProgram::createUniforms() {
  imageMaximaTexId_ = shaderManager_.getUniformLocation("imageLocalMaxima");
  /*imWid_ = shaderManager_.getUniformLocation("imW");
   imHid_ = shaderManager_.getUniformLocation("imH");*/
}
