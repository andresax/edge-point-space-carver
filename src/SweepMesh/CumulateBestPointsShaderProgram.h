/*
 * CumulatePointsShaderProgram.h
 *
 *  Created on: 16/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_CUMULATEBestPOINTSSHADERPROGRAM_H_
#define SWEEPMESH_CUMULATEBestPOINTSSHADERPROGRAM_H_

#include "ShaderProgram.h"
#include <glm.hpp>
#include <opencv2/core/core.hpp>
class CumulateBestPointsShaderProgram : public ShaderProgram {
public:
  CumulateBestPointsShaderProgram(int imageWidth, int imageHeight);
  virtual ~CumulateBestPointsShaderProgram();
  void initializeFramebufAndTex(GLuint &texId);
  void resetTex();

  void compute(bool renderFrameBuf = false);
  int collectNewPoints(std::vector<glm::vec4> &points);

  void setCurPointsImageTex(GLuint curPointsImageTex) {
    curPointsImageTex_ = curPointsImageTex;
  }
  GLuint getLastTex();

  void setCurIdCam2(GLuint curIdCam2) {
    this->curIdCam2 = curIdCam2;
  }

  void showLastImage();

private:
  void init();
  void initializeFramebufAndTexEven();
  void initializeFramebufAndTexOdd();

  void createAttributes();
  void createUniforms();
  void dumpTextureOnCPU();
  void collectPointsFromPixels();
  void collectPointsToImage(cv::Mat &outIm);


  GLuint framebufferEven_, framebufferOdd_, cumulativeId_, curPointsImageTex_, curPointsImageId_;
  /*attributes id*/
  GLuint posId_, texCoordId_;

  GLuint textureEvenTex_, textureOddTex_;
  GLboolean textureEven_, first_;

  GLfloat *pixels_;
  int ch_;
  GLuint curIdCam2, curIdCam2Id_;
  std::vector<glm::vec4> pointsAndIdNcc_;
};

#endif /* SWEEPMESH_CUMULATEBestPOINTSSHADERPROGRAM_H_ */
