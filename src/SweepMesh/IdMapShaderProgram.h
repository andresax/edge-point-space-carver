/*
 * IdMapShaderProgram.h
 *
 *  Created on: 14/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_IDMAPSHADERPROGRAM_H_
#define SWEEPMESH_IDMAPSHADERPROGRAM_H_

#include "ShaderProgram.h"
#include <glm.hpp>
#include <vector>

class IdMapShaderProgram : public ShaderProgram {
public:
  IdMapShaderProgram(int imageWidth, int imageHeight);
  virtual ~IdMapShaderProgram();
  void initializeFramebufAndTex(GLuint &texId);

  void compute(bool renderFrameBuf = false);

  void setAlpha(float alpha) {
    alpha_ = alpha;
  }

  void setDepthTexture(GLuint depthTexture) {
    depthTexture_ = depthTexture;
  }

  void setIdBuffer(GLuint idBuffer) {
    idBuffer_ = idBuffer;
  }

  void setMvp(const glm::mat4& mvp) {
    mvp_ = mvp;
  }

  void setNormalBuffer(GLuint normalBuffer) {
    normalBuffer_ = normalBuffer;
  }

private:
  void init();

  void createAttributes();
  void createUniforms();

  GLuint framebuffer_;
  /*uniforms id*/
  GLuint imageTexId_, mvpId_, shadowMapId_, mapIdid_;
  /*attributes id*/
  GLuint posId_;

  GLuint idAttrib_;
  GLuint idBuffer_, normAttrib_, normalBuffer_;
  float alpha_;
  GLuint alphaId_;
  GLuint depthTexture_;
  glm::mat4 mvp_;
};

#endif /* SWEEPMESH_IDMAPSHADERPROGRAM_H_ */
