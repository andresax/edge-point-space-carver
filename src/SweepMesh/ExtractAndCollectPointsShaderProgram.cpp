/*
 * ExtractAndCollectPointsShaderProgram.cpp
 *
 *  Created on: 14/apr/2015
 *      Author: andrea
 */

#include "ExtractAndCollectPointsShaderProgram.h"

ExtractAndCollectPointsShaderProgram::ExtractAndCollectPointsShaderProgram(int imageWidth, int imageHeight) :
    ShaderProgram(imageWidth, imageHeight) {
  pointsExtractedTexId_ = imagePointsKept_ = -1;
  framebuffer_ = imageTexId_ = idMap_ = 1;
  pointsExtractedTexId_ = idMapId_ = mvpId_ = -1;

  normAttrib_ = normalBuffer_ = idBuffer_ = -1;
  alphaId_ = -1;
  alpha_ = idAttrib_ = -1;
  posId_ = nccTex_ = imHid_ = imWid_ = -1;
  imageTexId_ = -1;
  /*attributes id*/
  posId_ = nccTex_ = framebuffer_ = -1;

  feedbackBuffer_ = feedbackLen_ = -1;

  query_ = -1;

}

ExtractAndCollectPointsShaderProgram::~ExtractAndCollectPointsShaderProgram() {
}

void ExtractAndCollectPointsShaderProgram::createTransformFeedback(int length) {
  glGenBuffers(1, &feedbackBuffer_);
  glBindBuffer(GL_ARRAY_BUFFER, feedbackBuffer_);
  glBufferData(GL_ARRAY_BUFFER, length * sizeof(glm::vec3), nullptr, GL_DYNAMIC_READ);
  feedbackLen_ = length;
}

void ExtractAndCollectPointsShaderProgram::compute(bool renderFrameBuf) {
  glClearColor(0.1666666666f, 0.1666666666f, 0.1666666666f, 0.0f);
  feedbackTrPoints_.assign(feedbackLen_, glm::vec4(0.0));

  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glViewport(0, 0, imageWidth_, imageHeight_);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glEnable(GL_CULL_FACE);

  shaderManager_.enable();

  glUniformMatrix4fv(mvpId_, 1, GL_FALSE, &mvp_[0][0]);
  glUniform1f(alphaId_, alpha_);
  glUniform1f(imWid_, static_cast<GLfloat>(imageWidth_));
  glUniform1f(imHid_, static_cast<GLfloat>(imageHeight_));

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, idMap_);
  glUniform1i(idMapId_, 0);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, imagePointsKept_);
  glUniform1i(imageTexId_, 1);

  glEnableVertexAttribArray(posId_);
  glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
  glVertexAttribPointer(posId_, 3, GL_FLOAT, GL_FALSE, 0, 0);

  glEnableVertexAttribArray(normAttrib_);
  glBindBuffer(GL_ARRAY_BUFFER, normalBuffer_);
  glVertexAttribPointer(normAttrib_, 3, GL_FLOAT, GL_FALSE, 0, 0);

  glEnableVertexAttribArray(idAttrib_);
  glBindBuffer(GL_ARRAY_BUFFER, idBuffer_);
  glVertexAttribPointer(idAttrib_, 1, GL_FLOAT, GL_FALSE, 0, 0);

  glEnable(GL_RASTERIZER_DISCARD);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, arrayBufferObj_);

  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, feedbackBuffer_);/*addd*/

  glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query_);
  glBeginTransformFeedback(GL_POINTS);
  if (useElements_Indices) {
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsBufferObj_);
    glDrawElements(GL_TRIANGLES, numElements_, GL_UNSIGNED_INT, 0);
  } else {
    glBindBuffer(GL_ARRAY_BUFFER, arrayBufferObj_);
    glDrawArrays(GL_TRIANGLES, 0, sizeArray_);
  }

  glEndTransformFeedback();
  glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

  glDisableVertexAttribArray(posId_);
  glDisableVertexAttribArray(normAttrib_);
  glDisableVertexAttribArray(idAttrib_);

  glFinish();
  glDisable(GL_RASTERIZER_DISCARD);

  GLuint primitives;
  glGetQueryObjectuiv(query_, GL_QUERY_RESULT, &primitives);
std::cout<<"Primitives read "<<primitives<<std::endl;
  feedbackTrPoints_.resize(primitives, glm::vec4(0.0));
  feedbackLen_= primitives;
  glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, feedbackLen_ * sizeof(glm::vec4), &feedbackTrPoints_[0]);

}

void ExtractAndCollectPointsShaderProgram::init() {
  shaderManager_.init();
  shaderManager_.addShader(GL_VERTEX_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/extract_collect_vertex_shader.glsl");
  shaderManager_.addShader(GL_GEOMETRY_SHADER, "/home/andrea/workspaceC/edgePointSpaceCarver/shaders/extract_collect_geometry_shader.glsl");
  shaderManager_.addFeedbackTransform("point");
  shaderManager_.finalize();
}

void ExtractAndCollectPointsShaderProgram::createAttributes() {
  posId_ = shaderManager_.getAttribLocation("position");
  normAttrib_ = shaderManager_.getAttribLocation("normal");
  idAttrib_ = shaderManager_.getAttribLocation("id");
}

void ExtractAndCollectPointsShaderProgram::createUniforms() {
  mvpId_ = shaderManager_.getUniformLocation("MVP");
  imageTexId_ = shaderManager_.getUniformLocation("imageMaxima");
  idMapId_ = shaderManager_.getUniformLocation("imageId");
  alphaId_ = shaderManager_.getUniformLocation("alpha");
  imWid_ = shaderManager_.getUniformLocation("imW");
  imHid_ = shaderManager_.getUniformLocation("imH");
}
