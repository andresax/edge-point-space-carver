/*
 * SweepShaderProgram.h
 *
 *  Created on: 01/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_SWEEPSHADERPROGRAM_H_
#define SWEEPMESH_SWEEPSHADERPROGRAM_H_

#include "ShaderProgram.h"
#include <glm.hpp>

class SweepShaderProgram : public ShaderProgram {
public:
  SweepShaderProgram(int imageWidth, int imageHeight);
  virtual ~SweepShaderProgram();
  void compute(glm::mat4 mvp, bool renderFrameBuf = false);
  void createTransformFeedback(int length);
  void updateTransformFeedback(int length);
  void updateVertexOnGPU();

  void setNormalBuffer(GLuint normalBuffer) {
    normalBuffer_ = normalBuffer;
  }

  void setAlpha(float alpha) {
    alpha_ = alpha;
  }

  void setCamCenter(const glm::vec3& camCenter) {
    camCenter_ = camCenter;
  }

  const std::vector<glm::vec3>& getFeedbackTr() const {
    return feedbackTr;
  }
private:
  void init();

  void createAttributes();
  void createUniforms();

  GLuint posAttrib_, normAttrib_;
  GLuint normalBuffer_;
  GLuint mvpID_, alphaId_, camCenterID_;
  glm::vec3 camCenter_;
  float alpha_;


  GLuint feedbackBuffer_;
  GLuint feedbackLength_;
  std::vector<glm::vec3> feedbackTr;

};

#endif /* SWEEPMESH_SWEEPSHADERPROGRAM_H_ */
