/*
 * NccShaderProgram.h
 *
 *  Created on: 08/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_NCCSHADERPROGRAM_H_
#define SWEEPMESH_NCCSHADERPROGRAM_H_

#include "ShaderProgram.h"

class NccShaderProgram : public ShaderProgram {
public:
  NccShaderProgram(int imageWidth, int imageHeight);
  NccShaderProgram(int imageWidth, int imageHeight, int window);
  virtual ~NccShaderProgram();
  void initTex();
  void populateTex(const cv::Mat &image);
  void initializeFramebufAndTex(GLuint &nccTexId);

  void compute(bool renderFrameBuf = false);

  void setImage2ReprojTex(GLuint image2ReprojTex) {
    image2ReprojTex_ = image2ReprojTex;
  }

  void setWindow(int window) {
    window_ = window;
  }

private:
  void init();

  void createAttributes();
  void createUniforms();

  GLuint framebuffer_;
  /*uniforms id*/
  GLuint imWid_, imHid_, image2ReprojTexId_, imageTexId_, wId_;
  /*tex id*/
  GLuint nccTexId_, image2ReprojTex_, imageTex_;
  /*attributes id*/
  GLuint posId_, texCoordId_;

  int window_;

};

#endif /* SWEEPMESH_NCCSHADERPROGRAM_H_ */
