/*
 * PointsCollectorPerPixel.cpp
 *
 *  Created on: 17/apr/2015
 *      Author: andrea
 */

#include "PointsCollectorPerPixel.h"

PointsCollectorPerPixel::PointsCollectorPerPixel() {
}

PointsCollectorPerPixel::~PointsCollectorPerPixel() {
}

void PointsCollectorPerPixel::resetPoints() {
  points_.clear();
  points_ = std::vector<glm::vec3>();
  visibility_.clear();
  visibility_ = std::vector<std::vector<int> >();
}

void PointsCollectorPerPixel::addPointsAndIdCam(const std::vector<glm::vec4>& pointsNccIdCam, const int id1) {
  glm::vec3 curPt;
  GLfloat curNcc, nccAndId;
  GLuint curIdCam;
  for (auto curPNI : pointsNccIdCam) {
    curPt = glm::vec3(curPNI.x, curPNI.y, curPNI.z);
    curIdCam = GLuint(glm::trunc(curPNI.w)); //decode the idx from w value
    std::vector<int> curVis;

    curVis.push_back(id1);
    curVis.push_back(curIdCam);
    visibility_.push_back(curVis);
    points_.push_back(curPt);
  }
}

const std::vector<glm::vec3>& PointsCollectorPerPixel::getActivefilteredPoints() {

  return points_;
}

const std::vector<std::vector<int> >& PointsCollectorPerPixel::getActiveVisibility() {
  return visibility_;
}
