/*
 * PointsCollector.cpp
 *
 *  Created on: 15/apr/2015
 *      Author: andrea
 */

#include "PointsCollector.h"
#include <iostream>
#include <algorithm>

PointsCollector::PointsCollector(int numTriangles) {
  numTriangles_ = numTriangles;
  resetPoints();
}

PointsCollector::~PointsCollector() {
}

void PointsCollector::resetPoints() {
  resetPoints(numTriangles_);
}

void PointsCollector::resetPoints(int numTriangles) {
  numTriangles_ = numTriangles;
  points_.assign(numTriangles, glm::vec3(0.0));
  filteredPoints_.assign(numTriangles, glm::vec3(0.0));
  visibility_.assign(numTriangles, std::vector<int>());
  medianX_.assign(numTriangles, std::vector<GLfloat>());
  medianY_.assign(numTriangles, std::vector<GLfloat>());
  medianZ_.assign(numTriangles, std::vector<GLfloat>());
  activefilteredPoints_.assign(numTriangles, glm::vec3(0.0));
  maxNcc_.assign(numTriangles, -10.0);
  kept_.assign(numTriangles, false);
  count_.assign(numTriangles, 0);
}
void PointsCollector::addPointsNccIds(const std::vector<glm::vec4>& pointsNccIds, const int id1, const int id2) {
  glm::vec3 curPt;
  GLfloat curNcc, nccAndId;
  GLuint curId;
  for (auto curPNI : pointsNccIds) {
    curPt = glm::vec3(curPNI.x, curPNI.y, curPNI.z);

    nccAndId = curPNI.w;
    curNcc = nccAndId - glm::trunc(nccAndId);
    curId = GLuint(glm::trunc(nccAndId));
//std::cout<<" nccAndId " <<nccAndId<<" curNcc " <<curNcc<<" curId " <<curId<<std::endl;
    if (curNcc > maxNcc_[curId]) {
      filteredPoints_[curId] = curPt;
      maxNcc_[curId] = curNcc;
      kept_[curId] = true;
      count_[curId]++;
      visibility_[curId].clear();
      visibility_[curId].push_back(id1);
      visibility_[curId].push_back(id2);
    }
  }

}

void PointsCollector::addPointsGrid(glm::mat4 mvp, const std::vector<glm::vec4>& pointsNccIds, const int id1, const int id2) {
  glm::vec3 curPt;
  GLfloat curNcc, nccAndId;
  GLuint curId;
  PointNccSweep p;

  for (auto curPNI : pointsNccIds) {
    p.point = glm::vec3(curPNI.x, curPNI.y, curPNI.z);
    nccAndId = curPNI.w;
    p.ncc = nccAndId - glm::trunc(nccAndId);
    p.idx1 = id1;
    p.idx2 = id2;

    g.addPoint(mvp, p);
  }
}

void PointsCollector::startGrid(int numRow, int numCol, int maxPt) {

  g.setNumTilesCol(numCol);
  g.setNumTilesRow(numRow);
  g.setMaxPoints(maxPt);
  g.reset();

}

void PointsCollector::addPointsNccMedian(const std::vector<glm::vec4>& pointsNccIds, const int id1, const int id2) {
  glm::vec3 curPt;
  GLfloat curNcc, nccAndId;
  GLuint curId;
  for (auto curPNI : pointsNccIds) {
    curPt = glm::vec3(curPNI.x, curPNI.y, curPNI.z);

    nccAndId = curPNI.w;
    curNcc = nccAndId - glm::trunc(nccAndId);
    curId = GLuint(glm::trunc(nccAndId));
//std::cout<<" nccAndId " <<nccAndId<<" curNcc " <<curNcc<<" curId " <<curId<<std::endl;

    medianX_[curId].push_back(curPt.x);
    medianY_[curId].push_back(curPt.y);
    medianZ_[curId].push_back(curPt.z);

    kept_[curId] = true;
    count_[curId]++;
    if (curNcc > maxNcc_[curId]) {
      maxNcc_[curId] = curNcc;
      visibility_[curId].clear();
      visibility_[curId].push_back(id1);
      visibility_[curId].push_back(id2);
    }
  }

  for (int cur = 0; cur < medianX_.size(); cur++) {
    if (kept_[cur] == true) {
      std::sort(medianX_[cur].begin(), medianX_[cur].end());
      std::sort(medianY_[cur].begin(), medianY_[cur].end());
      std::sort(medianZ_[cur].begin(), medianZ_[cur].end());
      int mid = round(medianZ_[cur].size() / 2);

      filteredPoints_[cur] = glm::vec3(medianX_[cur][mid], medianY_[cur][mid], medianZ_[cur][mid]);
    }
  }
}

std::vector<glm::vec3>& PointsCollector::getActivefilteredPoints() {
  activefilteredPoints_.assign(0, glm::vec3(0.0));

  for (int curP = 0; curP < numTriangles_; ++curP) {
    if (kept_[curP] && count_[curP] > 20) {
      activefilteredPoints_.push_back(filteredPoints_[curP]);
    }
  }

  return activefilteredPoints_;
}

const std::vector<std::vector<int> >& PointsCollector::getActiveVisibility() {
  activeVisibility_.clear();

  for (int curP = 0; curP < numTriangles_; ++curP) {
    if (kept_[curP] && count_[curP] > 20) {
      activeVisibility_.push_back(visibility_[curP]);
    }
  }

  return activeVisibility_;
}

std::vector<glm::vec3>& PointsCollector::getActivefilteredPointsFromGrid() {
  activefilteredPoints_.assign(0, glm::vec3(0.0));
  std::vector<PointNccSweep> points;
  g.getBestPoints(points);
  for (auto p : points) {

    activefilteredPoints_.push_back(p.point);
  }

  return activefilteredPoints_;
}

const std::vector<std::vector<int> >& PointsCollector::getActiveVisibilityFromGrid() {
  activeVisibility_.clear();
  std::vector<int> tmp;

  std::vector<PointNccSweep> points;
  g.getBestPoints(points);
  for (auto p : points) {
    tmp.clear();
    tmp.push_back(p.idx1);
    tmp.push_back(p.idx2);
    activeVisibility_.push_back(tmp);
  }

  return activeVisibility_;
}
