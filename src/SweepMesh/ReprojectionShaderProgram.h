/*
 * ReprojectionShaderProgram.h
 *
 *  Created on: 03/apr/2015
 *      Author: andrea
 */

#ifndef SWEEPMESH_REPROJECTIONSHADERPROGRAM_H_
#define SWEEPMESH_REPROJECTIONSHADERPROGRAM_H_

#include "ShaderProgram.h"
#include <glm.hpp>

class ReprojectionShaderProgram : public ShaderProgram {
public:
  ReprojectionShaderProgram(int imageWidth, int imageHeight);
  virtual ~ReprojectionShaderProgram();
  void initTex();
  void populateTex(const cv::Mat &image);

  void initializeFramebufAndTex(GLuint &imageReprojTex);

  void compute(bool renderFrameBuf = false);

  void setDepthTexture(GLuint depthTexture, GLuint depthTexture2) {
    depthTexture_ = depthTexture;
    depthTexture2_ = depthTexture2;
  }

  void setNormalBuffer(GLuint normalBuffer) {
    normalBuffer_ = normalBuffer;
  }

  void setAlpha(float alpha) {
    alpha_ = alpha;
  }

  void setMvp(const glm::mat4& mvp1, const glm::mat4& mvp2) {
    mvp1_ = mvp1;
    mvp2_ = mvp2;
  }

  void setCamCenter(const glm::vec3& camCenter) {
    camCenter_ = camCenter;
  }
private:
  void init();

  void createAttributes();
  void createUniforms();

  GLuint framebufferReproj_;
  GLuint imageReprojTex_;
  GLuint imageTex_;

  GLuint normAttrib_;
  GLuint normalBuffer_;
  GLuint alphaId_;
  float alpha_;

  GLuint posAttribReprojId_, mvp1IDReproj_, mvp2IDReproj_, image2TexId_, shadowMap1IdReproj_, shadowMap2IdReproj_;

  GLuint depthTexture_, depthTexture2_, camCenterID_;
  glm::vec3 camCenter_;

  glm::mat4 mvp1_, mvp2_;

};

#endif /* SWEEPMESH_REPROJECTIONSHADERPROGRAM_H_ */
