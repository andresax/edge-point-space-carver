/*
 * GridPoints.cpp
 *
 *  Created on: 28/mag/2015
 *      Author: andrea
 */

#include "GridPoints.h"

GridPoints::GridPoints() {

  numTilesCol_ = 1;
  numTilesRow_ = 1;

  maxPoints_ = 1;

}

GridPoints::~GridPoints() {
}

void GridPoints::addPoint(glm::mat4 mvp, PointNccSweep tmpPoint) {

  glm::vec4 pt2DH = mvp * glm::vec4(tmpPoint.point, 1.0);

  /*locate in grid*/

  /*convert to 0->0.9999 position*/
  glm::vec2 ofsettedPoints = glm::vec2(0.5 + 0.5 * pt2DH.x / pt2DH.w * 0.99999999999, //
  0.5 + 0.5 * pt2DH.y / pt2DH.w * 0.99999999999);

  glm::vec2 positioninGrid = glm::vec2(floor(ofsettedPoints.x * numTilesCol_), floor(ofsettedPoints.y * numTilesRow_));

  grid_[positioninGrid.y][positioninGrid.x].push_back(tmpPoint);


}

bool GridPoints::comparePoints(PointNccSweep p1, PointNccSweep p2) {
  return p1.ncc < p2.ncc;
}

void GridPoints::getBestPoints(std::vector<PointNccSweep> &points) {
  for (auto r : grid_) {
    for (auto c : r) {

      std::sort(c.begin(), c.end(), comparePoints);
      int numPt;

      if (c.size() < maxPoints_) {
        numPt = c.size();
      } else {
        numPt = maxPoints_;
      }

      if (numPt > 0) {
        points.insert(points.end(), c.begin(), c.begin() + numPt);
      }
    }
  }
}

void GridPoints::reset() {
  grid_.clear();
  std::vector<PointNccSweep> tmp1;
  std::vector<std::vector<PointNccSweep>> tmp2;
  tmp2.assign(numTilesCol_, tmp1);

  grid_.assign(numTilesRow_, tmp2);
}

