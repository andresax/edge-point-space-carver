/*
 * MIxedParser.cpp
 *
 *  Created on: 13/mar/2015
 *      Author: andrea
 */

#include "MixedParser.h"
#include "Parser.h"

MIxedParser::MIxedParser() {
  curCam_ = -1;
  numCameras_ = 0;
  numPoints_ = 0;

  fileStream_.open("/home/andrea/Documenti/PhD/parserbundler/herzu_15_04_07_rect.out", std::ios::in);
  fileStream2_.open("/home/andrea/Documenti/PhD/parserbundler/herzu_15_04_07_points.out", std::ios::in);

}

MIxedParser::~MIxedParser() {
  // TODO Auto-generated destructor stub
}

void MIxedParser::parse() {
  std::string line;

  //read num cam
  std::getline(fileStream_, line);
  std::istringstream iss(line);
  iss >> numCameras_;

  pointsVisibleFromCamN.assign(numCameras_, std::vector<int>());

  for (int curCam = 0; curCam < numCameras_; ++curCam) {
    CameraRect tempCamera;

    //rotation matrix <R>
    std::getline(fileStream_, line);
    iss.str(line);
    sscanf(iss.str().c_str(), "%f %f %f %f", &tempCamera.P(0, 0), &tempCamera.P(0, 1), &tempCamera.P(0, 2), &tempCamera.P(0, 3));
    //iss >> tempCamera.P(0,0) >> tempCamera.P(0,1) >>  tempCamera.P(0,2) >> tempCamera.P(0,3);
    std::getline(fileStream_, line);
    iss.str(line);
    sscanf(iss.str().c_str(), "%f %f %f %f", &tempCamera.P(1, 0), &tempCamera.P(1, 1), &tempCamera.P(1, 2), &tempCamera.P(1, 3));
    //    iss >> tempCamera.P(1,0) >> tempCamera.P(1,1) >>  tempCamera.P(1,2) >> tempCamera.P(1,3);
    std::getline(fileStream_, line);
    iss.str(line);
    std::string tempname;
    sscanf(iss.str().c_str(), "%f %f %f %f", &tempCamera.P(2, 0), &tempCamera.P(2, 1), &tempCamera.P(2, 2), &tempCamera.P(2, 3));
    //    iss >> tempCamera.P(2,0) >> tempCamera.P(2,1) >>  tempCamera.P(2,2) >> tempCamera.P(2,3);

    //translation vector <t>
    std::getline(fileStream_, line);
    iss.str(line);
    float a,b,c;
    iss >> a >> b >> c >> tempname;
    tempCamera.center[0] = a;
    tempCamera.center[1] = b;
    tempCamera.center[2] = c;
    tempCamera.names_.push_back(tempname);
camPaths_.push_back(tempname);
    cams_.push_back(tempCamera);
  }

  std::istringstream iss2;
  //read and discard the first line (# Bundle file v0.3)
  std::getline(fileStream2_, line);
  iss2.str(line);

  //read num cam
  std::getline(fileStream2_, line);
  iss2.str(line);
  iss2 >> numCameras_ >> numPoints_;

  camViewingPointN.assign(numPoints_, std::vector<int>());
  for (int curPoint = 0; curPoint < numPoints_; ++curPoint) {
    int lengthList, tempKey, tempIdx;
    float tempX, tempY;
    PointParser tempPoint;

    //point's position <x, y, z>
    std::getline(fileStream2_, line);
    iss2.clear();
    iss2.str(line);
    iss2 >> tempPoint.x >> tempPoint.y >> tempPoint.z;

    std::getline(fileStream2_, line);
    iss2.clear();
    iss2.str(line);
    //view list
    std::getline(fileStream2_, line);
    iss2.clear();
    iss2.str(line);
    iss2 >> lengthList;
    for (int curViewingCamera = 0; curViewingCamera < lengthList; ++curViewingCamera) {
      iss2 >> tempIdx;

      camViewingPointN[curPoint].push_back(curViewingCamera);
      pointsVisibleFromCamN[curViewingCamera].push_back(curPoint);

      iss2 >> tempKey;
      iss2 >> tempX;
      tempPoint.viewingCamerasX.push_back(tempX);
      iss2 >> tempY;
      tempPoint.viewingCamerasY.push_back(tempY);
    }
    cv::Point3d temp;
    temp.x = tempPoint.x;
    temp.y = tempPoint.y;
    temp.z = tempPoint.z;
    points_.push_back(temp);

  }
}

CameraRect& MIxedParser::getNextCam() {
    curCam_++;
    return cams_[curCam_];
}

std::vector<cv::Point3d>& MIxedParser::getCurPoints() {
  curPoints_.clear();
  if (curCam_ + 1 < numCameras_) {

    curCam_++;
    for(auto curPointIdx : pointsVisibleFromCamN[curCam_]){
      curPoints_.push_back(points_[curPointIdx]);
    }
  }
  return curPoints_;
}

